#!/bin/sh
# Run this script to *FULLY* clean up the source tree

if [ -e "Makefile" ]
then
	make clean
fi

rm -rf  *~			\
	aclocal.m4		\
	autom4te.cache/		\
	config.guess		\
	config.h		\
	config.h.in		\
	config.log		\
	config.status		\
	config.sub		\
	configure		\
	depcomp			\
	doc/*~			\
	include/*~		\
	install-sh		\
	libtool			\
	ltmain.sh		\
	m4/*~			\
	m4/libtool.m4		\
	m4/lt~obsolete.m4	\
	m4/ltoptions.m4		\
	m4/ltsugar.m4		\
	m4/ltversion.m4		\
	Makefile		\
	Makefile.in		\
	missing			\
	src/*~			\
	src/.deps/		\
	src/.libs/		\
	src/Makefile		\
	src/Makefile.in		\
	stamp-h1		\
	tests/*~

echo "\n>> zAlign source tree cleaned up successfully\n"
