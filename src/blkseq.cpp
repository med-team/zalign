/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include "blkseq.h"


/* Function definitions */

/*
 * blkseq_copy() copies 'st_blkseq' element from 'src' to 'dest'. It's worth
 * noting that arrays from 'src' are _literally_ copied to 'dest', not only
 * the array pointers.
 */
void blkseq_copy (blkseq_t dest, blkseq_t src) {
  size_t   size;

  /* size of all arrays that will be copied */
  size = src->end - src->start + 2;

  /* copy constant-sized fields */
  dest->start = src->start;
  dest->end   = src->end;

  /* copy variable-sized arrays */
  memcpy (dest->s,    src->s,    size * sizeof(simentry_t));
  memcpy (dest->p,    src->p,    size * sizeof(simentry_t));
  memcpy (dest->q,    src->q,    size * sizeof(simentry_t));
  memcpy (dest->hdiv, src->hdiv, size * sizeof(position_t));
  memcpy (dest->vdiv, src->vdiv, size * sizeof(position_t));

}


/*
 * blkseq_free() simply deallocates memory space occupied by 'blk'.
 */
void blkseq_free (blkseq_t blk) {

  /* housekeeping for variable-sized arrays... */
  free (blk->s);
  free (blk->p);
  free (blk->q);
  free (blk->hdiv);
  free (blk->vdiv);

  /* finally, free the block sequence itself */
  free (blk);

}


/*
 * blkseq_new() creates a new 'st_blkseq' element, 'blk', assigns appropriate
 * values to it and returns. It's worth noting that all arrays in 'blk' have
 * size equal to 'end - start + 2'.
 */
blkseq_t blkseq_new (position_t start, position_t end) {
  blkseq_t blk;
  size_t   size;

  /* allocate space for one 'st_blkseq' element */
  blk = (blkseq_t) malloc (sizeof(st_blkseq));

  /* size of all arrays to be allocated in this block sequence */
  size = end - start + 2;

  /* assign constant-sized fields */
  blk->start = start;
  blk->end   = end;

  /* allocate variable-sized arrays */
  blk->s    = (simentry_t*) malloc (size * sizeof(simentry_t));
  blk->p    = (simentry_t*) malloc (size * sizeof(simentry_t));
  blk->q    = (simentry_t*) malloc (size * sizeof(simentry_t));
  blk->hdiv = (position_t*) malloc (size * sizeof(position_t));
  blk->vdiv = (position_t*) malloc (size * sizeof(position_t));

  return blk;
}


/*
 * blkseq_recv() receives fields into block sequence 'blk' from source 'src'
 * through MPI messaging.
 */
void blkseq_recv (blkseq_t blk, int src) {
  MPI_Status status;
  position_t size;

  /* message size */
  size = blk->end - blk->start + 2;

  /* receive similarity values array 's' */
  MPI_Recv (blk->s, size, MPI_INT, src, 0, MPI_COMM_WORLD, &status);

  /* receive gap values arrays 'p' and 'q' */
  MPI_Recv (blk->p, size, MPI_INT, src, 0, MPI_COMM_WORLD, &status);
  MPI_Recv (blk->q, size, MPI_INT, src, 0, MPI_COMM_WORLD, &status);

  /* receive divergence arrays 'hdiv' and 'vdiv' */
  MPI_Recv (blk->hdiv, size, MPI_LONG, src, 0, MPI_COMM_WORLD, &status);
  MPI_Recv (blk->vdiv, size, MPI_LONG, src, 0, MPI_COMM_WORLD, &status);

}


/*
 * blkseq_send() sends all fields of block sequence 'blk' to destination 'dest'
 * through MPI messaging.
 *
 * Notice that synchronous sends are used to ensure that both sender and
 * receiver are at the same point in execution, and also because we don't want
 * to rely on system buffering, which could make the program non-portable or
 * to behave unexpectedly.
 */
void blkseq_send (blkseq_t blk, int dest) {
  position_t size;

  /* message size */
  size = blk->end - blk->start + 2;

  /* send similarity values array 's' */
  MPI_Ssend (blk->s, size, MPI_INT, dest, 0, MPI_COMM_WORLD);

  /* send gap values arrays 'p' and 'q' */
  MPI_Ssend (blk->p, size, MPI_INT, dest, 0, MPI_COMM_WORLD);
  MPI_Ssend (blk->q, size, MPI_INT, dest, 0, MPI_COMM_WORLD);

  /* send divergence arrays 'hdiv' and 'vdiv' */
  MPI_Ssend (blk->hdiv, size, MPI_LONG, dest, 0, MPI_COMM_WORLD);
  MPI_Ssend (blk->vdiv, size, MPI_LONG, dest, 0, MPI_COMM_WORLD);

}

