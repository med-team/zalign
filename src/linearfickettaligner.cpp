/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <assert.h>

#include "linearfickettaligner.h"

#define SIM(x,y)   (((*tSim)[x])       + (mK) - ((x)-(y)))
#define P(x,y)     (((*tP)[x])         + (mK) - ((x)-(y)))
#define Q(x,y)     (((*tQ)[x])         + (mK) - ((x)-(y)))
#define TRACE(x,y) (((*tTraceback)[x]) + (mK) - ((x)-(y)))

#define TRACEBACK_STOP     0x00
#define TRACEBACK_UP       0x01
#define TRACEBACK_DIAGONAL 0x02
#define TRACEBACK_LEFT     0x04

#define max2(a,b) ((a > b) ? a : b)
#define max3(a,b,c) ((a > b) ? max2(a,c) : max2(b,c))
#define max4(a,b,c,d) (max2(max2(a,b),max2(c,d)))


LinearFickettAligner::LinearFickettAligner(sequence_t s, sequence_t t, score_t sm, simentry_t D) {
  /* from aligner */
  this->s = s;
  this->t = t;
  this->sm = sm;

  /* from fickettaligner */
  mSim = NULL;
  mD = D;
  mProcessed = 0;

  /* already here in linearfickettaligner */
  strcpy(mLimitsPlotFilename, "");
  mSDisplacement = 0;
  mTDisplacement = 0;
  mK = strlen(s);
  mL = -strlen(t);
  mFindOnlyFirst = false;
  mAllocatedBytes = 0;
  m_sBestScore = 0;
  m_tBestScore = 0;
  m_BlockMemoryLimit = 64 * 1024 * 1024;
}


LinearFickettAligner::~LinearFickettAligner() {

  /* from fickettaligner */
  if (mSim != NULL) {
    free(mSim);
    mSim = NULL;
  }
}


void LinearFickettAligner::initializeFickettTable() {
  position_t i;
  position_t j;
  
  *SIM(0, 0) = 0;
  *P(0, 0) = 0;
  *Q(0, 0) = 0;
  *TRACE(0, 0) = TRACEBACK_STOP;

  // Encontra o ponto da primeira linha onde o score cai abaixo do recuper�vel
  for (i = 0,j = 1 ; j <= -mL; j++) {
    *SIM(0, j) = gap_open + j * gap_extension;
    *P(0, j) = 0;
    *Q(0, j) = gap_open + j * gap_extension;
    *TRACE(0, j) = TRACEBACK_LEFT;
  }
}


bool LinearFickettAligner::processLines(position_t startline, position_t endline) {  
  position_t i;
  position_t j;
  
  simentry_t up;
  simentry_t left;
  simentry_t diagonal;
  simentry_t p1;
  simentry_t p2;
  simentry_t q1;
  simentry_t q2;
  simentry_t score;
  simentry_t tr;

  // ponteiros para os valores sendo utilizados no calculo da similaridade
  simentry_t *sim;
  simentry_t *sim_u;
  simentry_t *sim_ul;
  simentry_t *sim_l;
  simentry_t *p;
  simentry_t *p_u;
  simentry_t *q;
  simentry_t *q_l;
  tracebackholder_t *trace;
      
  position_t i_minus_j;
  
  startline = (startline < 1) ? 1 : startline;

  using namespace std;
  
  for (i = startline; (i <= endline) && (!alignmentFound); i++) {
    position_t js, je;
    
    // Define as posicoes de inicio e fim da linha
    js = i - mK;
    js = (js < 1) ? 1: js;
    je = i - mL;
    je = (je > tlen) ? tlen : je;
    
    if (writePlot)
      fprintf(file, "%ld %ld %ld\n", i + mSDisplacement, js + mTDisplacement, je + mTDisplacement);
  
    tSim->reserve(i);
    tP->reserve(i);
    tQ->reserve(i);
    tTraceback->reserve(i);
  
    j = js;
    
    // Encontra que ponto da primeira coluna, o score cai abaixo do recuper�vel
    if (i <= mK) {
      *SIM(i,0) = gap_open + i * gap_extension;
      *P(i,0) = gap_open + i * gap_extension;
      *Q(i,0) = 0;
      *TRACE(i,0) = TRACEBACK_UP;
    }
    
    sim_ul = SIM(i-1,j-1);
    sim_u  = SIM(i-1,j);
    sim_l  = SIM(i,j-1);
    sim    = SIM(i,j);
    p_u    = P(i-1,j);
    p      = P(i,j);
    q_l    = Q(i,j-1);
    q      = Q(i,j);
    trace  = TRACE(i,j);

    for ( ; j <= je ; j++) {
      i_minus_j = i - j;

      /*************** Similarity ***************/
      diagonal = *sim_ul + ((seqs[i-1] == seqt[j-1]) ? match :mismatch);

      up = NEGATIVE_INFINITY;
      left = NEGATIVE_INFINITY;
      
      if ((i_minus_j) > mL) { // Est� na borda superior?
        /*************** Vertical gaps ***************/
        p1 = (*sim_u + gap_open);    // Opening
        p2 = (*p_u + gap_extension); // Extending
        up = *p = max(p1, p2);
      }

      if ((i_minus_j) < mK) { // Est� na borda inferior?
        /*************** Horizontal gaps ***************/
        q1 = (*sim_l + gap_open);    // Opening
        q2 = (*q_l + gap_extension); // Extending
        left = *q = max(q1, q2);
      }

      score = max3 (up, left, diagonal);

      *sim = score;
      
      // Define o valor do traceback
      tr = TRACEBACK_STOP;
      if (score == diagonal)
        tr = tr | TRACEBACK_DIAGONAL;

      if ((score == up) && (i_minus_j > mL))
        tr = tr | TRACEBACK_UP;

      if ((score == left) && (i_minus_j < mK))
        tr = tr | TRACEBACK_LEFT;

      *trace = tr;
      
      mProcessed++;
      
      // Se encontrou uma celula com valor igual ao melhor score, faz um traceback
      if (score == mD) {
        m_sBestScore = i;
        m_tBestScore = j;
          
        alignmentFound = true;
          
        return true;
      }
      
      //Avanca os ponteiros uma posicao a direita
      sim_ul++;
      sim_u++;
      sim_l++;
      sim++;
      p_u++;
      p++;
      q_l++;
      q++;
      trace++;
      
    }
  }
  return false;
}


alignment_t LinearFickettAligner::align() {
  using namespace std;
  
  position_t maxLen;
  position_t minLen;
  
  slen = strlen(this->s);
  tlen = strlen(this->t);
  
  alignmentFound = false;
  
  writePlot = (strlen(mLimitsPlotFilename) > 0);
  
  seqs = this->s;
  seqt = this->t;
      
  match = (*sm).match;
  mismatch = (*sm).mismatch;
  gap_open = (*sm).gap_open;
  gap_extension = (*sm).gap_extension;
  

  tSim = new LinearFickettTable<simentry_t>(mK, mL);
  tP = new LinearFickettTable<simentry_t>(mK, mL);
  tQ = new LinearFickettTable<simentry_t>(mK, mL);
  tTraceback = new LinearFickettTable<tracebackholder_t>(mK, mL);
  
  int m_BlockLines;
  int linesPerExtension;
  int extesionMemoryLimit;
  
  m_BytesPerLine = (sizeof(simentry_t) * 3 + sizeof(tracebackholder_t)) * ((mK - mL) + 1);
  
  extesionMemoryLimit = m_BlockMemoryLimit / 128;
  
  m_BlockLines = m_BlockMemoryLimit / m_BytesPerLine;
  m_BlockLines = (m_BlockLines < 1) ? 1 : m_BlockLines;
  m_BlockLines = (m_BlockLines > (((signed int)slen) + 1)) ? (slen + 1) : m_BlockLines;
  
  linesPerExtension =  extesionMemoryLimit / m_BytesPerLine;
  linesPerExtension = (linesPerExtension > (m_BlockLines / 2)) ? (m_BlockLines / 2) : linesPerExtension;
  linesPerExtension = (linesPerExtension < 1) ? 1 : linesPerExtension;
  linesPerExtension = (linesPerExtension > (((signed int)slen) + 1)) ? (slen + 1) : linesPerExtension;
  
  assert (m_BlockLines >= (linesPerExtension * 2));
  
  tSim->setLinesPerExtension(linesPerExtension);
  tP->setLinesPerExtension(linesPerExtension);
  tQ->setLinesPerExtension(linesPerExtension);
  tTraceback->setLinesPerExtension(linesPerExtension);
  
  tSim->reserve(2);
  tP->reserve(2);
  tQ->reserve(2);
  tTraceback->reserve(2);
  
  initializeFickettTable();

  maxLen = mK - mL;
  minLen = maxLen;
  
  if (writePlot)
    file = fopen(mLimitsPlotFilename, "w");
  
  // Processa a matriz de similaridades 
  int block;
  
  for (block = 1; ; block++) {
    position_t block_start;
    position_t block_end;
    bool alignmentFound;
    
    block_start = ((block - 1) * m_BlockLines) + 1; 
    block_end = block * m_BlockLines;
    block_end = (block_end > slen) ? slen : block_end;
    
    alignmentFound = processLines(block_start, block_end);

    if (alignmentFound)
      break;

    else {
      tSim->freeTop(block_end - 1);
      tP->freeTop(block_end - 1);
      tQ->freeTop(block_end - 1);
      tTraceback->freeTop(block_end - 1);
    }
  }

  alignment_t alignment;
  alignment = traceback(m_sBestScore, m_tBestScore);
  mBestAlignmentVector.push_back(alignment);
  mCount = mBestAlignmentVector.size();
  
  if (writePlot)
    fclose(file);
  
  mAllocatedBytes = tTraceback->getAllocatedBytes() 
                    + tQ->getAllocatedBytes()
                    + tP->getAllocatedBytes()
                    + tSim->getAllocatedBytes();
  
  delete tTraceback;
  delete tQ;
  delete tP;
  delete tSim;
  return NULL;
}


alignment_t  LinearFickettAligner::traceback(position_t i, position_t j) {
  position_t tlen;
  tracebackholder_t lasttrace;
  position_t spos;
  position_t tpos;
    
  tlen = strlen(t);
  alignment_t a = alignment_new (this->s , this->t);

  /* COMMENT THIS -- complement to match long version of 'new Alignment' */
  a->s_start = i + 1;
  a->s_end   = i;
  a->t_start = j + 1;
  a->t_end   = j;
  a->score   = *SIM(i,j);
  /* END COMMENT THIS */

  spos = i;
  tpos = j;
  lasttrace = TRACEBACK_DIAGONAL;
  
  while((spos != 0) || (tpos != 0)) {
    using namespace std;
 
    if (!tTraceback->isAllocated(spos)) {
      position_t cacheLine;
      
      cacheLine = tSim->raiseCache(spos);
      cacheLine = tP->raiseCache(spos);
      cacheLine = tQ->raiseCache(spos);
      cacheLine = tTraceback->raiseCache(spos);

      alignmentFound = false;      
      processLines(cacheLine + 1, spos);
      
      tSim->freeBottom(spos + 1);
      tP->freeBottom(spos + 1);
      tQ->freeBottom(spos + 1);
      tTraceback->freeBottom(spos + 1);
    }
    
    if (lasttrace == TRACEBACK_UP) {
      if (*TRACE(spos, tpos) & TRACEBACK_UP) {
        alignment_add (a, PATH_UP); spos--; lasttrace = TRACEBACK_UP;
      } 
      else if (*TRACE(spos, tpos) & TRACEBACK_DIAGONAL) {
        alignment_add (a, PATH_DIAG); spos--; tpos--; lasttrace = TRACEBACK_DIAGONAL;
      } 
      else if (*TRACE(spos, tpos) & TRACEBACK_LEFT) {
        alignment_add (a, PATH_LEFT); tpos--; lasttrace = TRACEBACK_LEFT;
      }
    }

    else if (lasttrace == TRACEBACK_LEFT) {
      if (*TRACE(spos, tpos) & TRACEBACK_LEFT) {
        alignment_add (a, PATH_LEFT); tpos--; lasttrace = TRACEBACK_LEFT;
      } 
      else if (*TRACE(spos, tpos) & TRACEBACK_DIAGONAL) {
        alignment_add (a, PATH_DIAG); spos--; tpos--; lasttrace = TRACEBACK_DIAGONAL;
      } 
      else if (*TRACE(spos, tpos) & TRACEBACK_UP) {
        alignment_add (a, PATH_UP); spos--; lasttrace = TRACEBACK_UP;
      }
    }

    else {
      if (*TRACE(spos, tpos) & TRACEBACK_DIAGONAL) {
        alignment_add (a, PATH_DIAG); spos--; tpos--; lasttrace = TRACEBACK_DIAGONAL;
      } 
      else if (*TRACE(spos, tpos) & TRACEBACK_UP) {
        alignment_add (a, PATH_UP); spos--; lasttrace = TRACEBACK_UP;
      } 
      else if (*TRACE(spos, tpos) & TRACEBACK_LEFT) {
        alignment_add (a, PATH_LEFT); tpos--; lasttrace = TRACEBACK_LEFT;
      }
    }
  }
  using namespace std;
  
  return a;
}


alignment_t LinearFickettAligner::getAlignment( count_t index ) {
  return mBestAlignmentVector.at(index);
}

              
float LinearFickettAligner::getProcessed() {
  return (float)mProcessed / (float)(m_sBestScore * m_tBestScore);
}

