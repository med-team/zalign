/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include "zalign.h"


/* Function definitions */

/*
 * zalign_stage1() is the first stage the 'zalign' executable, analogous to the
 * second stage of 'mpialign'. It finds the best alignment scores of the
 * inverted input sequences 's' and 't' using a blocked aligner algorithm, then
 * iterates over the obtained list of alignments executing zalign_stage2().
 */
void zalign_stage1 (sequence_t s, sequence_t t, score_t scores) {
  alignbounds_t bounds;
  blkalign_t    blkaligner;
  sequence_t    rs,
                rt;

  /* show introductory information for zalign_stage1() */
  /* TODO: show a progress meter on this stage         */
  printf ("Stage 1: Find best scores\n");
  printf ("-------------------------\n\n\n");

  /* invert input sequences */
  rs = sequence_invert (s);
  rt = sequence_invert (t);

  /* initialize blocked aligner and align inverted sequences */
  blkaligner = blkalign_new (BLKALIGN_STD, rs, rt, scores, NULL, 0, 0);
  blkalign_align (blkaligner);

  /* store pointer to alignment array's first element */
  bounds = blkaligner->strat->alignarray->alignments;

  /* show introductory information for zalign_stage2() */
  printf ("Stage 2: Obtain alignments\n");
  printf ("--------------------------\n\n");

  /* iterate through each alignment array element, executing zalign_stage2() */
  do {
    zalign_stage2 (bounds, s, t, scores);
    bounds = bounds->next;
  } while (bounds != NULL);

  /* housekeeping... */
  blkalign_free (blkaligner);  
  free (rt);
  free (rs);

}


/*
 * zalign_stage2() is the second stage the 'zalign' executable, analogous to the
 * fourth stage of 'mpialign'. It finds and prints an alignment of 's' and 't'
 * sequences, given by the 'bounds' parameter. No value is returned.
 */
void zalign_stage2 (alignbounds_t bounds, sequence_t s, sequence_t t, score_t scores) {
  alignment_t alignment;
  sequence_t  ss,
              st;
  size_t      slen,
              tlen;

  /* calculate sequence sizes */
  slen = strlen (s);
  tlen = strlen (t);

  /* obtain subsequences to be aligned */
  ss = sequence_subseq (s, slen - bounds->s_end + 1, slen);
  st = sequence_subseq (t, tlen - bounds->t_end + 1, tlen);

  /* initialize linear fickett aligner, set parameters and align subsequences */
  LinearFickettAligner aligner (ss, st, scores, bounds->score);
  aligner.setK (-bounds->vdiv);
  aligner.setL (-bounds->hdiv);
  aligner.setSDisplacement (slen - bounds->s_end);
  aligner.setTDisplacement (tlen - bounds->t_end);
  aligner.align ();

  /* print obtained alignment to screen */
  alignment = aligner.getAlignment (0);
  SET_DISPLACEMENT (alignment->s_start, alignment->s_end,
                    (slen - bounds->s_end));
  SET_DISPLACEMENT (alignment->t_start, alignment->t_end,
                    (tlen - bounds->t_end));
  alignment_display (alignment, s, t);

  /* housekeeping... */
  alignment_free (alignment);
  free (st);
  free (ss);
  
}


/*
 * zalign entrypoint
 */
int main(int argc, char *argv[]) {
  sequence_t s,
             t;
  st_options options = {"", "", 1, 1, 1, NULL};

  /* parse commandline options, exiting on error */
  if (opt_parse (argc, argv, ZALIGN_RANK, &options))
    return EXIT;

  /* read sequences from filesystem */
  s = fasta_read (options.sfile);
  t = fasta_read (options.tfile);

  /* print some informational stuff before we begin */
  printf ("\nAlignment configuration\n");
  printf ("-----------------------\n\n");

  printf (" # File parameters\n\n");
  printf ("     S sequence: %u characters\n", strlen(s));
  printf ("     Filename:   %s\n\n", options.sfile);
  printf ("     T sequence: %u characters\n", strlen(t));
  printf ("     Filename:   %s\n\n", options.tfile);

  printf (" # Scoring parameters\n\n");
  printf ("     Match:        %3d\n", options.scores->match);
  printf ("     Mismatch:     %3d\n", options.scores->mismatch);
  printf ("     Gap Opening:  %3d\n", options.scores->gap_open);
  printf ("     Gap Extension:%3d\n\n\n", options.scores->gap_extension);

  /* execute first stage of alignment -- it'll call second stage later on */
  zalign_stage1 (s, t, options.scores);

  /* housekeeping... */
  free (s);
  free (t);
  score_free (options.scores);

  return SUCCESS;
}

