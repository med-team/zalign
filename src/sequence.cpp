/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include "sequence.h"


/* Function definitions */

/*
 * TODO: solve the bunch of 'strlen(seq)' that were inserted in the code as a
 *       temporary solution for the now-nonexistent 'Sequence' class.
 */

/*
 * sequence_bcast() broadcasts sequence 'seq' from the root node to all other
 * nodes in MPI_COMM_WORLD, using 'rank' to distinguish which code should be
 * executed in each node.
 */
sequence_t sequence_bcast (sequence_t seq, int rank) {
  position_t  len;

  /* root node calculates the length of the sequence, plus trailing NUL character */
  if (rank == ROOT_RANK)
    len = strlen(seq) + 1;

  /* and broadcasts it to the other nodes */
  MPI_Bcast (&len, 1, MPI_INT, ROOT_RANK, MPI_COMM_WORLD);

  /* the other nodes allocate memory to hold the sequence */
  if (rank != ROOT_RANK)
    seq = (sequence_t) malloc (len * sizeof(char));

  /* sequence is sent via broadcast to all nodes */
  MPI_Bcast (seq, len, MPI_CHAR, ROOT_RANK, MPI_COMM_WORLD);

  return seq;
}


/*
 * sequence_invert() allocates a new sequence 'invseq' with the same size of
 * parameter 'seq', copies characters from 'seq' into 'invseq' in reverse order
 * and returns.
 */
sequence_t sequence_invert (sequence_t seq) {
  position_t  i,
              len;
  sequence_t  invseq;

  /* allocate 'strlen(seq) + 1' bytes to 'invseq' */
  len = strlen(seq);
  invseq = (sequence_t) malloc ((len + 1) * sizeof(char));

  /* iterate through 'seq' in reverse order, copying characters to 'invseq' */
  for (i = 0 ; i < len ; i++)
    invseq[i] = seq[len - 1 - i];

  /* don't forget trailing NUL character! */
  invseq[i] = '\0';

  return invseq;
}


/*
 * sequence_subseq() allocates a new sequence 'subseq' with '(start - end) + 2'
 * size -- or the opposite, depending on which one is larger -- then copies
 * characters from 'seq', 'start' to 'end', and returns the subsequence
 * obtained.
 */
sequence_t sequence_subseq (sequence_t seq, position_t start, position_t end) {
  position_t  i,
              len,
              pos;
  sequence_t  subseq;
  signed char step;
  
  /* are we dealing with a regular or inverted subsequence? */
  step = ((start < end) ? 1 : -1);

  /* calculate length of subsequence */
  len  = ((start < end) ? (end - start) : (start - end)) + 1;

  /* allocate 'len + 1' bytes to 'subseq' */
  subseq = (sequence_t) malloc ((len + 1) * sizeof(char));

  /* iterate through 'seq' in 'step' order and 'len' size, copying to 'subseq' */
  for (i = 0, pos = (start - 1) ; i < len ; i++, pos += step)
    subseq[i] = seq[pos];

  /* don't forget trailing NUL character! */
  subseq[i] = '\0';

  return subseq;
}

