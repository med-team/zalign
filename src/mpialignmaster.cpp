/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include <stdlib.h>
#include <iostream>
#include <mpi.h>

#include "alignbounds.h"
#include "alignment.h"
#include "mpialignmaster.h"
#include "sequence.h"


extern char *root_BoundsArray;
extern count_t root_BufferCount;
extern int comm_size;
extern int comm_rank;

count_t dispatchedJobs;
count_t busySlaves;
char *currentBound;

extern sequence_t s;
extern sequence_t t;
 
using namespace std;

void sendCommandTo(int command, int to) {

  MPI_Send(&command, 1, MPI_UNSIGNED, to, 0, MPI_COMM_WORLD);
}


int waitRequest() {
  MPI_Status status;
  char dummy;

  MPI_Recv(&dummy, 1, MPI_CHAR, MPI_ANY_SOURCE, TAG_JOB_REQUEST, MPI_COMM_WORLD, &status);

  return status.MPI_SOURCE;
}


alignment_t receiveAlignment() {
  alignment_t alignment;

  alignment = alignment_new (s, t);
  alignment_recv (alignment, TAG_ALIGNMENT_ANSWER);

  return alignment;
}


void dispatchJobToSlave (int slave, alignbounds_t bounds) {

  sendCommandTo(COMMAND_ALIGN, slave);
  alignbounds_send (bounds, slave);

  busySlaves++;
  dispatchedJobs++;
}


void dispatchJobToMaster (alignbounds_t bounds) {
  alignment_t alignment;
  size_t size;
  
  alignment = align(bounds, &size);
  alignment_display (alignment, s, t);
  alignment_free (alignment);
  
  dispatchedJobs++;
}


void *nextBound() {
  void *temp;
  temp = currentBound;

  currentBound += (sizeof(st_alignbounds));
  
  return temp;
}


/**
 * @brief Mestre da 2a fase do alinhador
 */
void master() {
  alignbounds_t bounds;
  
  dispatchedJobs = 0;
  busySlaves = 0;
  currentBound = root_BoundsArray;
  
  //Obtem os alinhamentos a partir dos bordas delimitadas    
  while (dispatchedJobs < root_BufferCount) {
    while(busySlaves < ((count_t)comm_size - 1)) {
      int slave;

      bounds = ((alignbounds_t) nextBound());
      slave = waitRequest();
      dispatchJobToSlave(slave, bounds);
      
      if (dispatchedJobs >= root_BufferCount)
        break;
    }

    if (dispatchedJobs < root_BufferCount) {
      bounds = ((alignbounds_t) nextBound());
      dispatchJobToMaster(bounds);
    }

    while (busySlaves > 0) {
      alignment_t alignment;

      alignment = receiveAlignment();
      alignment_display (alignment, s, t);
      alignment_free (alignment);
      busySlaves--;
    }
  }
  
  int i;
  for (i = (comm_size - 1); i > 0; i--) {
    int slave;
    slave = waitRequest();
    sendCommandTo(COMMAND_TERMINATE, slave);
  }

  free(root_BoundsArray);

}

