/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include "blkstrat.h"


/* Macro definitions */

#define MAX2(a,b)     ((a > b) ? a : b)
#define MIN2(a,b)     ((a < b) ? a : b)
#define MAX4(a,b,c,d) (MAX2 (MAX2(a,b), MAX2(c,d)))


/* Function definitions */

/*
 * blkstrat_align() aligns a block sequence horizontally delimited by
 * 'strat->inborder->row->end - strat->inborder->row->start + 1' and vertically
 * by 'strat->inborder->column->end - strat->inborder->column->start + 1'.
 *
 * To achieve this, it uses pertinent chunks of sequences 'strat->s' and
 * 'strat->t', and scoring values contained in 'strat->scores'.
 *
 * If, while aligning, it finds an alignment with score equal to or greater
 * than 'strat->alignarray->best_score', then that alignment is added to the
 * best alignment array 'strat->alignarray'. Note that the alignment is added
 * with start/end values reverted to avoid obtaining different tracebacks in
 * program's fourth stage.
 *
 * After iterating through all block rows, output row and column are stored in
 * 'strat->outborder' for use in the next blkstrat_align() call. No value is
 * returned.
 */
void blkstrat_align (blkstrat_t strat) {
  alignbounds_t  bounds;
  blkseq_t       curr_row,
                 prev_row,
                 tmp_row;
  position_t    *hdiv,
                *hdiv_u,
                *hdiv_ul,
                *hdiv_l,
                 height,
                 i,
                 i_minus_j,
                 j,
                *vdiv,
                *vdiv_u,
                *vdiv_ul,
                *vdiv_l,
                 width;
  scoreval_t     match,
                 mismatch,
                 gap_open,
                 gap_extension;
  sequence_t     s,
                 t,
                 seq_s,
                 seq_t;
  simentry_t     best_score,
                 diagonal,
                 p1,
                 p2,
                *p,
                *p_u,
                 q1,
                 q2,
                *q,
                *q_l,
                *sim,
                *sim_u,
                *sim_ul,
                *sim_l;

  /* get best score obtained so far */
  best_score = strat->alignarray->best_score;

  /* retrieve scoring values to be assigned */
  match         = strat->scores->match;
  mismatch      = strat->scores->mismatch;
  gap_open      = strat->scores->gap_open;
  gap_extension = strat->scores->gap_extension;

  /* retrieve appropriate chunks of sequences */
  seq_s  = strat->s;
  seq_s += strat->inborder->column->start - 1;
  seq_t  = strat->t;
  seq_t += strat->inborder->row->start - 1;

  /* calculate block width and height */
  width  = (strat->inborder->row->end    - strat->inborder->row->start)    + 1;
  height = (strat->inborder->column->end - strat->inborder->column->start) + 1;

  /* assign working and output rows */
  prev_row = strat->inborder->row;
  curr_row  = strat->outborder->row;

  s = seq_s;

  /* iterate through all block rows */
  for (i = 1 ; i <= height ; i++) {

    /* initialize pointers */
    sim_ul = prev_row->s;
    sim_u  = prev_row->s + 1;
    sim_l  = curr_row->s;
    sim    = curr_row->s + 1;

    p_u    = prev_row->p + 1;
    p      = curr_row->p + 1;

    q_l    = curr_row->q;
    q      = curr_row->q + 1;

    hdiv_ul = prev_row->hdiv;
    hdiv_u  = prev_row->hdiv + 1;
    hdiv_l  = curr_row->hdiv;
    hdiv    = curr_row->hdiv + 1;

    vdiv_ul = prev_row->vdiv;
    vdiv_u  = prev_row->vdiv + 1;
    vdiv_l  = curr_row->vdiv;
    vdiv    = curr_row->vdiv + 1;

    t = seq_t;

    /* copy input border data to current row */
    *sim_l = strat->inborder->column->s[i];
    *q_l   = strat->inborder->column->q[i];

    *hdiv_l = strat->inborder->column->hdiv[i];
    *vdiv_l = strat->inborder->column->vdiv[i];
  
    i_minus_j = (i + strat->inborder->column->start - 1) - (strat->inborder->row->start);

    /* now, iterate through all block columns in this row */    
    for (j = 1 ; j <= width ; j++) {

      /* calculate similarity value */
      diagonal = *sim_ul + ((*s == *t) ? match : mismatch);

      /* apply vertical gap penalty */
      p1 = (*sim_u + gap_open);      /* gap-open */
      p2 = (*p_u   + gap_extension); /* gap-extension */
      *p = MAX2 (p1, p2);            /* which one of these has the best score? */

      /* apply horizontal gap penalty */
      q1 = (*sim_l + gap_open);      /* gap-open */
      q2 = (*q_l   + gap_extension); /* gap-extension */
      *q = MAX2 (q1, q2);            /* which one of these has the best score? */

      /* choose best score between available options and zero */
      *sim = MAX4 (diagonal, *p, *q, 0);

      /* calculate divergence */
      *hdiv = *vdiv = (i_minus_j);

      if (*sim != 0) {
        if (*sim == diagonal) {
          *hdiv = *hdiv_ul;
          *vdiv = *vdiv_ul;
        }

        if (*sim == *p) {
          *hdiv = MAX2(*hdiv, *hdiv_u);
          *vdiv = MIN2(*vdiv, *vdiv_u);
        }

        if (*sim == *q) {
          *hdiv = MAX2(*hdiv, *hdiv_l);
          *vdiv = MIN2(*vdiv, *vdiv_l);
        }
      }

      /* if the score obtained so far is equal or greater than our best score... */
      if (*sim >= best_score) {
        /* allocate a new 'st_alignbounds' structure */
        bounds = alignbounds_new ();

        /* assign values to our new 'bounds' structure -- note */
        /* that the alignment is added in an inverted fashion  */
        bounds->s_start = 1;
        bounds->t_start = 1;
        bounds->s_end   = i + strat->inborder->column->start - 1;
        bounds->t_end   = j + strat->inborder->row->start - 1;
        bounds->score   = *sim;
        bounds->hdiv    = *hdiv - i_minus_j;
        bounds->vdiv    = *vdiv - i_minus_j;

        /* add 'bounds' to our array of best alignments */
        alignarray_add (strat->alignarray, bounds);

        /* update 'best_score' counter */
        best_score = *sim;
      }

      /* advance pointers one position to the right and continue */
      sim_ul++;
      sim_u++;
      sim_l++;
      sim++;

      p_u++;
      p++;

      q_l++;
      q++;

      hdiv_ul++;
      hdiv_u++;
      hdiv_l++;
      hdiv++;

      vdiv_ul++;
      vdiv_u++;
      vdiv_l++;
      vdiv++;

      i_minus_j--;

      t++;
    }

    /* write data produced after this run to output column */
    strat->outborder->column->s[i]    = *(sim-1);
    strat->outborder->column->p[i]    = *(p-1);
    strat->outborder->column->q[i]    = *(q-1);
    strat->outborder->column->hdiv[i] = *(hdiv-1); 
    strat->outborder->column->vdiv[i] = *(vdiv-1); 

    /* change working rows */
    tmp_row  = prev_row;
    prev_row = curr_row;
    curr_row = tmp_row;

    s++;
  }
  
  /* undo last change, since we had already finished */
  strat->inborder->row   = curr_row;
  strat->outborder->row  = prev_row;
  strat->outborder->type = OUTPUT_BORDER;
  
  /* copy last input column value to first output column */
  strat->outborder->column->s[0]    = strat->inborder->row->s[width];
  strat->outborder->column->p[0]    = strat->inborder->row->p[width];
  strat->outborder->column->q[0]    = strat->inborder->row->q[width];
  strat->outborder->column->hdiv[0] = strat->inborder->row->hdiv[width];
  strat->outborder->column->vdiv[0] = strat->inborder->row->vdiv[width];

  /* copy last input row value to first output row */
  strat->outborder->row->s[0]    = strat->inborder->column->s[height];
  strat->outborder->row->p[0]    = strat->inborder->column->p[height];
  strat->outborder->row->q[0]    = strat->inborder->column->q[height];
  strat->outborder->row->hdiv[0] = strat->inborder->column->hdiv[height];
  strat->outborder->row->vdiv[0] = strat->inborder->column->vdiv[height];

}


/*
 * blkstrat_free() simply deallocates memory space occupied by 'strat' and its
 * alignment array 'strat->alignarray'.
 */
void blkstrat_free (blkstrat_t strat) {

  /* deallocate memory occupied by alignment array */
  alignarray_free (strat->alignarray);

  /* and block strategy itself */
  free (strat);

}


/*
 * blkstrat_initcolumn() initializes column 'col' for use with Gotoh's
 * algorithm implemented by blkstrat_align(), using scoring parameters from
 * 'strat'.
 */
void blkstrat_initcol (blkstrat_t strat, blkseq_t col) {
  position_t  i;

  /* iterate through column size, initializing fields */
  for (i = 1 ; i <= (col->end - col->start + 1) ; i++) {
    col->s[i]    = 0;
    col->p[i]    = strat->scores->gap_open + ((col->start + i - 1) * strat->scores->gap_extension);
    col->q[i]    = 0;
    col->hdiv[i] = col->start + i - 1;
    col->vdiv[i] = col->start + i - 1;
  }

}


/*
 * blkstrat_initrow() initializes row 'row' for use with Gotoh's
 * algorithm implemented by blkstrat_align(), using scoring parameters from
 * 'strat'.
 */
void blkstrat_initrow (blkstrat_t strat, blkseq_t row) {
  position_t  j;

  /* iterate through row size, initializing fields */
  for (j = 1 ; j <= (row->end - row->start + 1) ; j++) {
    row->s[j]    = 0;
    row->p[j]    = 0;
    row->q[j]    = strat->scores->gap_open + ((row->start + j - 1) * strat->scores->gap_extension);
    row->hdiv[j] = -(row->start + j - 1);
    row->vdiv[j] = -(row->start + j - 1);
  }

}


/*
 * blkstrat_new() creates a new 'st_blkstrat' element, 'strat', creates a new
 * alignment array for use with the block strategy and returns.
 */
blkstrat_t blkstrat_new (void) {
  blkstrat_t strat;

  /* allocate space for one 'st_blkstrat' element */
  strat = (blkstrat_t) malloc (sizeof(st_blkstrat));

  /* create a new alignment array for our strategy */
  strat->alignarray = alignarray_new ();

  return strat;
}

