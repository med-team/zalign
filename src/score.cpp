/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include "score.h"


/* Function definitions */

/*
 * score_free() simply deallocates memory space occupied by 'score'.
 */
void score_free (score_t scores) {

  free (scores);

}


/*
 * score_new() creates a new 'st_scores' element, 'scores', assigns score
 * values passed as parameters to the function and returns.
 */
score_t score_new (scoreval_t match, scoreval_t mismatch, scoreval_t gap_open, scoreval_t gap_extension) {
  score_t scores;

  /* allocate space for one 'st_scores' element */
  scores = (score_t) malloc (sizeof(st_scores));

  /* assign score values and return */
  scores->match         = match;
  scores->mismatch      = mismatch;
  scores->gap_open      = gap_open;
  scores->gap_extension = gap_extension;

  return scores;
}

