/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifdef HAVE_CONFIG_H
  #include <config.h>
#endif

#include <stdlib.h>
#include <iomanip>
#include <iostream>
#include <mpi.h>
#include <time.h>

#include "alignarray.h"
#include "alignment.h"
#include "blkalign.h"
#include "blkstrat.h"
#include "fastareader.h"
#include "linearfickettaligner.h"
#include "mpialign.h"
#include "mpialignmaster.h"
#include "mpialignslave.h"
#include "opt.h"
#include "score.h"


/* @file mpialign.cpp
 * @brief Rotina principal em Paralelo
 * @todo Implementar as excecoes para uma correta manipulacao dos erros
 * @todo Verificar sistematicamente as assercoes e necessidade de invariantes */
using namespace std;


/* Global variables */

char                      *root_BoundsArray = NULL;       /* array with alignment borders after sending to the 'root' processor */
count_t                    local_Count;                   /* amount of alignments in each processor */
count_t                   *root_CountArray = NULL; 		    /* array containing the number of alignments found in each processor */
alignbounds_t              local_Alignment;               /* temporary object to manipulate alignment borders */
blkstrat_t                 strategy;                      /* alignment strategy */
int                        comm_rank;                     /* rank of this processor in the COMM_WORLD group */
int                        comm_size;                     /* size of the COMM_WORLD group */
blkalign_t                 aligner;                       /* MPI alignments */
sequence_t                 s;                             /* S sequence */
sequence_t                 t;                             /* T sequence */
simentry_t                 global_BestScore;              /* best global score */
simentry_t                 local_BestScore;               /* best local score of each processor */
simentry_t                *root_BestScoreArray = NULL;    /* array containing the best scores found in each processor */
size_t                     local_BufferSize;              /* size in bytes of the serialized alignment borders array of each processor */
size_t                     root_BufferCount;              /* size in number of components of the serialized alignment borders array */
size_t                    *root_BufferSizeArray = NULL;   /* array with buffer sizes needed to store each alignment list per processor */
size_t                     root_BufferSize;               /* size in bytes of the serialized alignment borders array */
st_options                 options = {"", "", 1, 10, 10, NULL}; /* struct to hold options passed to the program, initialized to default values */


/* @brief Produz o alinhamento a partir de um objeto DivergenceAlignmentbounds->
 * @param bounds Objeto DivergenceAlignmentBound contendo as informa��nes 
 * @param size Ponteiro para uma vari��nel size_t que conter��no tamanho da regi��n
 * de memria alocada para armazenar a representa��no serializada do alinhamento.
 * @return Ponteiro para um objeto Alignment j��nserializado. A regi��n de memria
 * retornada deve ser liberada chamando-se a fun��no free().
 * @todo Mudar os argumentos desta fun��no, pos @a size j��nest��npresente como o
 * primeiro dado na representa��no serializada do objeto. */
alignment_t align (alignbounds_t bounds, size_t *size) {
  alignment_t alignment;
  sequence_t  ss,
              st;
  size_t      slen,
              tlen;

  /* calculate sequence sizes */
  slen = strlen (s);
  tlen = strlen (t);

  ss = sequence_subseq (s, slen - bounds->s_end + 1, slen);
  st = sequence_subseq (t, tlen - bounds->t_end + 1, tlen);

  LinearFickettAligner aligner(ss, st, options.scores, bounds->score);
  aligner.setK(-bounds->vdiv);
  aligner.setL(-bounds->hdiv);
  aligner.setSDisplacement(slen - bounds->s_end);
  aligner.setTDisplacement(tlen - bounds->t_end);
  aligner.setFindOnlyFirst(true);
  aligner.align();
  
  assert(aligner.count() == 1);

  alignment = aligner.getAlignment(0);
  SET_DISPLACEMENT (alignment->s_start, alignment->s_end,
                    (slen - bounds->s_end));
  SET_DISPLACEMENT (alignment->t_start, alignment->t_end,
                    (tlen - bounds->t_end));

  free (st);
  free (ss);
  
  return alignment; 
}

/* @brief Junta os objetos de defini��no de alinhamento referentes ao melhor 
 * score global. 
 * Ao final, o processador raiz possuir��numa lista dos melhores alinhamentos
 * encontrados. */
void gatherBestScores (void) {
  char        *local_BoundsArray,
              *root_CurrentBounds,
              *tmp;
  count_t      i;
  MPI_Status   status;
  double       starttime,
               endtime;

  local_BoundsArray = NULL;

  /* if root node, show introductory information for mpialign_stage3() */
  if (comm_rank == ROOT_RANK) {
    printf ("Stage 3: Gather best scores\n");
    printf ("---------------------------\n\n");
  }

  MPI_Barrier(MPI_COMM_WORLD);
  starttime = MPI_Wtime();
  
  local_Count = aligner->strat->alignarray->count;
  local_BestScore = 0;
  local_BufferSize = 0;
  
  /* Junta a quantidade de scores encontrados em cada no */
  if (comm_rank == 0) {
    root_CountArray = (count_t *)malloc(comm_size * sizeof(count_t));
    root_BestScoreArray = (simentry_t *)malloc(comm_size * sizeof(simentry_t));
    root_BufferSizeArray = (size_t *)malloc(comm_size * sizeof(size_t));
  }

  /* Junta os melhores scores de cada no */
  if (local_Count > 0)
    local_BestScore = aligner->strat->alignarray->best_score;

  /* Junta os tamanhos de cada array caso estes devam ser transferidos */
  local_BufferSize = aligner->strat->alignarray->count * sizeof(st_alignbounds);

  MPI_Gather(&local_Count, 1, MPI_UNSIGNED, root_CountArray, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
  MPI_Gather(&(local_BestScore), 1, MPI_INT, root_BestScoreArray, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Gather(&(local_BufferSize), 1, MPI_INT, root_BufferSizeArray, 1, MPI_INT, 0, MPI_COMM_WORLD);
  
  /* Exibe os dados capturados */
  if (comm_rank == 0) {
    for (i = 0; i < (count_t)comm_size; i++) {
      cout << i << ": local_Count=" 
          << root_CountArray[i] 
          << " score=" 
          << root_BestScoreArray[i] 
          << " local_BufferSize=" 
          << root_BufferSizeArray[i]<< endl;
    }
  }

  /* Encontra qual o melhor score e faz broadcast a todos os nos */
  global_BestScore = 0;
  root_BufferSize = 0;
  root_BufferCount = 0;

  if (comm_rank == 0) {
    for (i = 0; i < (count_t)comm_size; i++) {
      if (global_BestScore == root_BestScoreArray[i]) {
        root_BufferSize += root_BufferSizeArray[i];
        root_BufferCount += root_CountArray[i];
      }
      if (global_BestScore < root_BestScoreArray[i]) {
        global_BestScore = root_BestScoreArray[i];
        root_BufferSize = root_BufferSizeArray[i];
        root_BufferCount = root_CountArray[i];
      }
    }
  }

  MPI_Bcast(&global_BestScore, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);

  /* Despeja os local_Alignment em um buffer para que estes sejam transferidos por MPI */

  if (local_BestScore == global_BestScore) {    
    local_BoundsArray = (char*) malloc (local_BufferSize);
    tmp = local_BoundsArray;

    local_Alignment = aligner->strat->alignarray->alignments;

    do {
      memcpy (tmp, local_Alignment, sizeof(st_alignbounds));
      tmp += sizeof(st_alignbounds);
      local_Alignment = local_Alignment->next;
    } while (local_Alignment != NULL);
  }
  
  /* Recebe os limites dos nos */
  if (comm_rank == 0) {
    root_BoundsArray = (char *)malloc(root_BufferSize);    
    root_CurrentBounds = root_BoundsArray;
    
    if (root_BestScoreArray[0] == global_BestScore) {
      memcpy(root_CurrentBounds, local_BoundsArray, root_BufferSizeArray[0]);
      root_CurrentBounds += root_BufferSizeArray[0];
    }

    for (i = 1; i < (count_t)comm_size; i++) {
      if (root_BestScoreArray[i] == global_BestScore) {
        MPI_Recv ((void *)root_CurrentBounds, root_BufferSizeArray[i], MPI_CHAR, i, 0, MPI_COMM_WORLD, &status);
        root_CurrentBounds += root_BufferSizeArray[i];
      }
    }
  }
  else {
    if (local_BestScore == global_BestScore)
      MPI_Send(local_BoundsArray, local_BufferSize, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
  }

  /* Libera o buffer local onde foram guardados os local_Alignment */
  if(local_BestScore == global_BestScore)
    free(local_BoundsArray);  
  
  /* Libera espaco alocado pelo mestre */
  if (comm_rank == 0) {
    free(root_BufferSizeArray);
    free(root_BestScoreArray);
    free(root_CountArray);
  }
  
  MPI_Barrier(MPI_COMM_WORLD);
  endtime = MPI_Wtime();

  /* if root node, print elapsed time running mpialign_stage3() */
  if (comm_rank == 0) {
    printf ("Alignments found: %d\n", root_BufferCount);
    printf ("Elapsed time: %f sec\n\n\n", endtime - starttime);
  }

  blkalign_free (aligner);

}


/* @brief Produz os alinhamentos a partir dos dados e scores obtidos na primeira
 * fase. */
void createAlignments (void) {
  double starttime,
         endtime;

  /* if root node, show introductory information for mpialign_stage4() */
  if (comm_rank == ROOT_RANK) {
    printf ("Stage 4: Obtain alignments\n");
    printf ("--------------------------\n\n");
  }

  MPI_Barrier(MPI_COMM_WORLD);
  starttime = MPI_Wtime();
  
  /* Produz os alinhamentos efetivamente. */
  if (comm_rank == 0)  
    master();
  else
    slave();
  
  MPI_Barrier(MPI_COMM_WORLD);
  endtime = MPI_Wtime();

  /* if root node, print elapsed time running mpialign_stage4() */
  if (comm_rank == ROOT_RANK)
    printf ("Elapsed time: %f sec\n\n\n", endtime - starttime);

}


/* 
 * mpialign_stage2() ...
 */
void mpialign_stage2 (void) {
  double     starttime,
             endtime;
  sequence_t rs,        /* inverted S sequence */
             rt;        /* inverted T sequence */

  /* if root node, show introductory information for mpialign_stage2() */
  if (comm_rank == ROOT_RANK) {
    printf ("Stage 2: Find best scores\n");
    printf ("-------------------------\n\n");
  }

  /* synchronize all nodes */
  MPI_Barrier(MPI_COMM_WORLD);

  /* get mpialign_stage2() execution start time */
  starttime = MPI_Wtime();

  /* invert input sequences */
  rs = sequence_invert (s);  
  rt = sequence_invert (t);

  /* initialize blocked aligner and align inverted sequences */
  aligner = blkalign_new (BLKALIGN_MPI, rs, rt, options.scores, &options, comm_rank, comm_size);
  blkalign_align (aligner);

  /* resync all nodes */
  MPI_Barrier(MPI_COMM_WORLD);

  /* get mpialign_stage2() execution end time */
  endtime = MPI_Wtime();

  /* housekeeping... */
  free (rs);
  free (rt);

  /* if root node, print elapsed time running mpialign_stage2() */
  if (comm_rank == ROOT_RANK)
    printf ("Elapsed time: %f sec\n\n\n", endtime - starttime);

}


/* 
 * mpialign_stage1() ...
 */
void mpialign_stage1 (void) {

  /* if root node, show introductory information for mpialign_stage1() */
  if (comm_rank == ROOT_RANK) {
    printf ("\nStage 1: Distribute data to all nodes\n");
    printf ("-------------------------------------\n\n");
  }

  /* broadcast gathered data to cluster nodes */
  s = sequence_bcast (s, comm_rank);
  t = sequence_bcast (t, comm_rank);

  /* before receiving option parameters, all nodes but */
  /* the root one have to initialize scoring structure */
  if (comm_rank != ROOT_RANK)
    options.scores = score_new (DEFAULT_MATCH_SCORE, DEFAULT_MISMATCH_SCORE,
                                DEFAULT_GAPOPEN_SCORE, DEFAULT_GAPEXTENSION_SCORE);

  /* ok, now we're ready to transmit option parameters */
  opt_bcast (&options);

  /* print some informational stuff before we begin */
  if (comm_rank == ROOT_RANK) {
    printf (" # File parameters\n\n");
    printf ("     S sequence: %u characters\n", strlen(s));
    printf ("     Filename:   %s\n\n", options.sfile);
    printf ("     T sequence: %u characters\n", strlen(t));
    printf ("     Filename:   %s\n\n", options.tfile);

    printf (" # Performance parameters\n\n");
    printf ("     Number of 'split' submatrices:%3d\n", options.split);
    printf ("     Available nodes:              %3d\n", comm_size);
    printf ("     Horizontal block divisions:   %3d\n", options.hblk);
    printf ("     Vertical block divisions:     %3d", options.vblk);

    /* check if 'options.vblk' parameter was forced */
    if (options.split > 1)
      printf (" (forced)");

    printf ("\n\n");

    printf (" # Scoring parameters\n\n");
    printf ("     Match:        %3d\n", options.scores->match);
    printf ("     Mismatch:     %3d\n", options.scores->mismatch);
    printf ("     Gap Opening:  %3d\n", options.scores->gap_open);
    printf ("     Gap Extension:%3d\n\n\n", options.scores->gap_extension);
  }

}


/*
 * mpialign entrypoint
 */
int main(int argc, char *argv[]) {
  char   err_flag;
  double starttime,
         endtime;

  /* initialize error flag to neutral state */
  err_flag = SUCCESS;

  /* initialize MPI environment */
  MPI_Init (&argc, &argv);
  MPI_Comm_rank (MPI_COMM_WORLD, &comm_rank);
  MPI_Comm_size (MPI_COMM_WORLD, &comm_size);

  /* get overall execution start time */
  starttime = MPI_Wtime();

  /* root node only */
  if (comm_rank == ROOT_RANK) {
    /* parse commandline options, setting flag on error */
    err_flag = ((opt_parse (argc, argv, comm_rank, &options)) ? EXIT : SUCCESS);

    /* if no parsing errors were found... */
    if (err_flag != EXIT) {
      /* read sequences from filesystem */
      s = fasta_read (options.sfile);
      t = fasta_read (options.tfile);

      /* if using cyclic block model, set 'vblk' to a sane value */
      if (options.split > 1)
        options.vblk = (count_t) comm_size;
    }
  }

  /* communicate to all nodes if any parsing error was found */
  MPI_Bcast (&err_flag, 1, MPI_CHAR, ROOT_RANK, MPI_COMM_WORLD);

  /* if so, terminate MPI environment and exit; else, keep going */
  if (err_flag)
    goto TERMINATE;

  /*  */
  mpialign_stage1 ();

  /*  */
  mpialign_stage2 ();

  /* TODO: change this function name to mpialign_stage3() */
  gatherBestScores();

  /* TODO: and this one to mpialign_stage4(), why not? */
  createAlignments();

  /* housekeeping... */
  free (s);
  free (t);
  score_free (options.scores);

  /* get overall execution end time */
  endtime = MPI_Wtime();

  /* if root node, print total time elapsed */
  if (comm_rank == ROOT_RANK)
    printf ("Total elapsed time: %f sec\n\n", endtime - starttime);

TERMINATE:
  /* terminate MPI environment */
  MPI_Finalize();

  return (err_flag == EXIT) ? EXIT : SUCCESS;
}

