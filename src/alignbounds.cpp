/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include "alignbounds.h"


/* Function definitions */

/*
 * alignbounds_free() simply deallocates memory space occupied by 'bounds'.
 */
void alignbounds_free (alignbounds_t bounds) {

  /* housekeeping... */
  free (bounds);

}


/*
 * alignbounds_new() creates a new 'st_alignbounds' element, 'bounds', assigns
 * appropriate values to it and returns.
 */
alignbounds_t alignbounds_new (void) {
  alignbounds_t bounds;

  /* allocate space for one 'st_alignbounds' element */
  bounds = (alignbounds_t) malloc (sizeof(st_alignbounds));

  /* make sure we're not pointing to any bogus place */
  bounds->next = NULL;

  return bounds;
}


/*
 * alignbounds_recv() receives an 'st_alignbounds' structure into 'bounds' from
 * source 'src' through MPI messaging. Note that 'bounds' must be preallocated
 * before calling this function.
 */
void alignbounds_recv (alignbounds_t bounds, int src) {
  MPI_Status status;

  MPI_Recv (bounds, sizeof(st_alignbounds), MPI_CHAR, src, 0, MPI_COMM_WORLD, &status);

}


/*
 * alignbounds_send() sends 'st_alignbounds' structure 'bounds' to destination
 * 'dest' through MPI messaging.
 */
void alignbounds_send (alignbounds_t bounds, int dest) {

  MPI_Send (bounds, sizeof(st_alignbounds), MPI_CHAR, dest, 0, MPI_COMM_WORLD);
  
}

