/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include "blkborder.h"


/* Function definitions */

/*
 * blkborder_free() simply deallocates memory space occupied by 'border'.
 */
void blkborder_free (blkborder_t border) {

  free (border);

}


/*
 * blkborder_new() creates a new 'st_blkborder' element, 'border', assigns
 * 'type' as its border type and returns.
 */
blkborder_t blkborder_new (bordertype_t type) {
  blkborder_t border;

  /* allocate space for one 'st_blkborder' element */
  border = (blkborder_t) malloc (sizeof(st_blkborder));

  /* assign block border type and return */
  border->type  = type;

  return border;
}

