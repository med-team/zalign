/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include "fastareader.h"


/* Function definitions */

/*
 * fasta_read() opens FASTA file 'file', ignores comments and concatenates
 * its DNA sequence in 'seq' and returns the obtained sequence string.
 *
 * TODO: deal with complex FASTA files
 */
sequence_t fasta_read (char *file) {
  FILE        *fp;
  long int     size;
  sequence_t   buf,
               seq;
  size_t       pos;
  struct stat  sb;

  /* open 'file' in 'fp' */
  if ((fp = fopen (file, "r")) == NULL)
    opt_panic (file);

  /* get file statistics for 'fp' */
  if ((fstat(fileno(fp), &sb)) != 0)
    opt_panic ("fstat");

  /* allocate 'sizeof(fp)' bytes to 'seq' */
  if ((seq = (sequence_t) malloc ((size_t) sb.st_size)) == NULL)
    opt_panic("malloc");

  /* use optimal blocksize for I/O on reads */
  size = sb.st_blksize;

  /* discard first line of FASTA file */
  if ((fgets(seq, size, fp)) == NULL) {
    if (ferror(fp))
      opt_panic ("fgets");
  }

  /* point 'buf' to the start of 'seq' */
  buf = seq;

  /* TODO: optimize file reading technique, using fgets() isn't so smart */
  while (!feof(fp)) {
    if (fgets(buf, size, fp) != NULL) {

      if (ferror(fp)) {
        opt_panic ("fgets");
      }

      /* find terminating newline in stream, swap it with a
         NUL character and point 'buf' to this location */
      else {
        pos = strcspn(buf, "\n");
        buf[pos] = '\0';
        buf += pos;
      }

    }
  }

  fclose (fp);

  return seq;
}

