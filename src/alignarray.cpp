/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include "alignarray.h"


/* Function definitions */

/*
 * alignarray_add() adds a new element 'bounds' to 'array's list of alignments.
 * First, it checks if 'bounds' has a better score than our best score so far,
 * case in which it clears the list before adding 'bounds' to it. If that's not
 * the case, 'bounds' is added to the beggining of the list and 'array's
 * variables are updated accordingly.
 *
 * Alignment array is implemented as a one-way linked stack -- the last element
 * added to the stack is inserted on top of it, and linked to the rest of the
 * stack through the 'bounds->next' pointer.
 */
void alignarray_add (alignarray_t array, alignbounds_t bounds) {

  /* check if 'bounds' has a better score than our best score so far -- if */
  /* that's the case, clear all elements from the array and set the score  */
  if (bounds->score > array->best_score) {
    alignarray_clear (array);
    array->best_score = bounds->score;
  }

  /* add 'bounds' as 'array's first element, linking to the rest of the list */
  bounds->next      = array->alignments;
  array->alignments = bounds;

  /* increment number of elements in alignment array by 1 */
  array->count++;

}


/*
 * alignarray_clear() clears all alignments in 'array', freeing memory occupied
 * by those elements, and reinitializes array counters.
 */
void alignarray_clear (alignarray_t array) {
  alignbounds_t tmp;

  /* while we don't reach the end of the list, 'free' */
  /* each element and jump to the next one            */
  while (array->alignments != NULL) {
    tmp = array->alignments->next;
    alignbounds_free (array->alignments);
    array->alignments = tmp;
  }

  /* reset counters to initial state */
  array->best_score = 1;
  array->count      = 0;

}


/*
 * alignarray_free() simply deallocates memory space occupied by 'array'.
 */
void alignarray_free (alignarray_t array) {

  /* housekeeping... */
  free (array);

}


/*
 * alignarray_new() creates a new 'st_alignarray' element, 'array', assigns
 * appropriate values to it and returns.
 */
alignarray_t alignarray_new (void) {
  alignarray_t array;

  /* allocate space for one 'st_alignarray' element */
  array = (alignarray_t) malloc (sizeof(st_alignarray));

  /* make sure we're not pointing to any bogus place */
  array->alignments = NULL;

  /* initialize array best score and number of elements */
  array->best_score = 1;
  array->count      = 0;

  return array;
}

