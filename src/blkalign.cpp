/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include "blkalign.h"


/* Global variables */

blkseq_t  lockcol;  /* buffer column used to avoid deadlock conditions */


/* Function definitions */

/*
 * blkalign_align() aligns two sequences 'aligner->strat->s' and
 * 'aligner->strat->t' using a block alignment strategy 'aligner->strat'.
 * There are two versions of the algorithm: the standalone one is intended for
 * use in non-cluster environments, and the parallel version for clusters.
 *
 * To achieve the goal, sequences are arranged to form an alignment matrix,
 * which is aligned in a single run (standalone version) or partitioned in
 * blocks configured by several parameters, being each of these block aligned
 * separately (parallel version). Block-size configuration parameters are, 
 * namely: number of cluster nodes and commandline options 'hblk' and 'vblk'.
 *
 * The best alignments' bounds obtained during alignment are stored in
 * 'aligner->strat->alignarray', which also contains the number of alignments
 * found and their score. Note that the actual alignments are NOT obtained,
 * only their boundaries. No value is returned.
 */
void blkalign_align (blkalign_t aligner) {
  blkborder_t  inborder,
               outborder;
  blkseq_t     tmpcol,
              *tmprow;
  count_t      i,
               j,
               k;

  /* initialize counter variables */
  i = j = k = 0;

  /* if root node, inform we've started processing */
  if (aligner->rank == ROOT_RANK)
    blkalign_msg (aligner, START, k, i, j);

  /* create new temporary borders */
  inborder  = blkborder_new (INPUT_BORDER);
  outborder = blkborder_new (OUTPUT_BORDER);

  /* allocate input/output rows and columns */
  blkalign_allocborders (aligner);

  /* iterate through all 'split' submatrices */
  for (k = 0 ; k < aligner->split ; k++) {
    /* if root node, inform we've started a new 'split' submatrix */
    if (aligner->rank == ROOT_RANK)
      blkalign_msg (aligner, SPLIT, k, i, j);

    /* if running parallel version, set i/o rows starting and ending positions */
    /* -- std version has only one row, so no need to set those                */
    if (aligner->type == BLKALIGN_MPI)
      blkalign_setrow (aligner, k);

    /* initialize input rows */
    blkalign_premx (aligner);

    /* iterate through all vertical subdivisions */
    for (i = 0 ; i < aligner->vblk ; i++) {
      /* if running parallel version, set i/o column starting and ending       */
      /* positions -- std version has only one column, so no need to set those */
      if (aligner->type == BLKALIGN_MPI)
        blkalign_setcol (aligner, i);

      /* pre-first-block (in this vertical subdiv) handle */
      blkalign_preblk (aligner, k);

      /* if processing a new 'split' submatrix, except */
      /* the first one, execute release-lock handle    */
      if ((k != 0) && (i == 0))
        blkalign_sendlock (aligner);

      /* iterate through all horizontal subdivisions in this vertical subdiv */
      for (j = 0 ; j < aligner->hblk ; j++) {
        /* set input border for this block */
        inborder->type    = INPUT_BORDER;
        inborder->row     = aligner->inrows[j];
        inborder->column  = aligner->incol;

        /* set output border for this block */
        outborder->type   = OUTPUT_BORDER;
        outborder->row    = aligner->outrows[j];
        outborder->column = aligner->outcol;

        /* align this block */
        aligner->strat->inborder  = inborder;
        aligner->strat->outborder = outborder;
        blkstrat_align (aligner->strat);

        /* copy border changes made during alignment to original array */
        aligner->inrows[j]  = inborder->row;
        aligner->outrows[j] = outborder->row;

        /* swap border columns */
        tmpcol          = aligner->incol;
        aligner->incol  = aligner->outcol;
        aligner->outcol = tmpcol;

        /* if root node, inform we've finished this 'hblk' */
        if (aligner->rank == ROOT_RANK)
          blkalign_msg (aligner, BLOCK, k, i, j + 1);
      }

      /* swap border columns again at the end of this vertical subdiv */
      tmpcol          = aligner->incol;
      aligner->incol  = aligner->outcol;
      aligner->outcol = tmpcol;

      /* swap rows */
      tmprow           = aligner->inrows;
      aligner->inrows  = aligner->outrows;
      aligner->outrows = tmprow;

      /* post-last-block (in this vertical subdiv) handle */
      blkalign_postblk (aligner, k, i);

    }
  }

  /* if root node, inform we're finished */
  if (aligner->rank == ROOT_RANK)
    blkalign_msg (aligner, END, k, i, j + 1);

  /* housekeeping... */
  blkborder_free (inborder);
  blkborder_free (outborder);

}


/*
 * blkalign_allocborders() allocates input/output rows and columns that will be
 * used to process the alignment matrix. Starting and ending positions of each
 * row and column are set later by blkalign_setrow() and blkalign_setcol()
 * functions, respectively.
 *
 * If using the parallel version of the algorithm, 'aligner->strat->t' sequence
 * is first split in equal parts configured by 'aligner->split'. Then, the
 * sequence is equally split between all cluster nodes, and again as defined by
 * 'aligner->hblk' parameter. The resulting value defines block width.
 *
 * Still in the parallel version, 'aligner->strat->s' sequence is divided in
 * equal parts configured by 'aligner->vblk', defining block height.
 *
 * Since all related values ('aligner->split', 'aligner->size', 'aligner->hblk'
 * and 'aligner->vblk') are equal to 1 in the standalone version of the
 * algorithm, both block height and width are equal to full sequence sizes,
 * resulting in a single block to be processed.
 */
void blkalign_allocborders (blkalign_t aligner) {
  count_t    j;

  /* allocate space for input/output row pointers -- on 'zalign', */
  /* there's only one 'inrow' and one 'outrow'                    */
  aligner->inrows  = (blkseq_t*) malloc (sizeof(blkseq_t) * aligner->hblk);
  aligner->outrows = (blkseq_t*) malloc (sizeof(blkseq_t) * aligner->hblk);

  /* allocate input/output row block sequences -- blkalign_setrow() */
  /* will set their start/end positions correctly, later on         */
  for (j = 0 ; j < aligner->hblk ; j++) {
    aligner->inrows[j]  = blkseq_new (1, aligner->blk_width);
    aligner->outrows[j] = blkseq_new (1, aligner->blk_width);
  }

  /* allocate input/output column block sequences -- blkalign_setcol() */
  /* will set their start/end positions correctly, later on            */
  aligner->incol   = blkseq_new (1, aligner->blk_height);
  aligner->outcol  = blkseq_new (1, aligner->blk_height);

  /* allocate buffer column used to avoid deadlock conditions */
  lockcol = blkseq_new (1, aligner->blk_height);

}


/*
 * blkalign_free() deallocates memory space occupied by each input/output row
 * in 'aligner', input/output rows and columns themselves, block alignment
 * strategy using during alignment, and finally, 'aligner' structure.
 */
void blkalign_free (blkalign_t aligner) {
  count_t j;

  /* deallocate memory occupied by each input/output row */
  /* BUG: segfaults if 'hblk == 1' */
  for (j = 0 ; j < aligner->hblk ; j++)
    blkseq_free (aligner->inrows[j]);

  for (j = 0 ; j < aligner->hblk ; j++)
    blkseq_free (aligner->outrows[j]);

  /* deallocate memory occupied by input/output row containers themselves */
  free (aligner->inrows);
  free (aligner->outrows);

  /* deallocate memory occupied by input/output columns */
  blkseq_free (aligner->incol);
  blkseq_free (aligner->outcol);

  /* deallocate memory occupied by buffer column */
  blkseq_free (lockcol);

  /* deallocate memory occupied by block strategy */
  blkstrat_free (aligner->strat);

  /* and block aligner itself */
  free (aligner);

}


/*
 * blkalign_msg() simply prints informational messages according to 'type',
 * showing how much of the alignment matrix has been calculated so far.
 */
void blkalign_msg (blkalign_t aligner, char type, int split_pass, int vblk_pass, int hblk_pass) {
  short int asterisk,
            i,
            j,
            perc;

  switch (type) {
    /* if 'type == START', show starting message */
    case START:
      printf ("  Showing progress information for root node (rank 0):");
      fflush (stdout);
      break;

    /* if 'type == SPLIT', show we've started a new 'split' submatrix */
    case SPLIT:
      printf ("\n");
      printf ("    split %*d/%d: |                                        |   0%%",\
              aligner->psplit, split_pass + 1, aligner->split);
      fflush (stdout);
      break;

    /* if 'type == BLOCK', update progressbar to show block processing progress */
    case BLOCK:
      /* calculate completed percentage */
      perc     = ceilf((float)(((aligner->hblk * vblk_pass) + (hblk_pass)) * 100) / (float)(aligner->hblk * aligner->vblk));

      /* and how many asterisks should be printed to the progressbar */
      asterisk = ceilf((float)(perc * 40) / (float)100);

      /* update progressbar */
      for (i = 0 ; i < 46       ; i++) printf ("\b");
      for (i = 0 ; i < asterisk ; i++) printf ("*");
      for (j = i ; j < 40       ; j++) printf (" ");
      printf ("| %3d%%", perc);
      fflush (stdout);
      break;

    /* if 'type == END', jump some lines and exit */
    /* TODO: show ETA for last node to finish     */
    case END:
      printf ("\n\n");
      printf ("  Waiting for remaining nodes to finish...\n\n");
      break;
  }

}


/*
 * blkalign_new() creates a new 'st_blkalign' element, 'aligner', and assigns
 * its block aligner 'type'. Then, it checks if that type requests creation of
 * a parallel version of the block aligner or not, initializing variables
 * accordingly. Finally, it creates a new strategy 'strat' for use with the
 * block aligner, initializes strategy and aligner variables and returns.
 *
 * It's worth noting that, as described by this module's header file
 * (blkalign.h), there are two types of blocked aligner, BLKALIGN_STD and
 * BLKALIGN_MPI. They operate in similar ways, with a few differences:
 *
 * The standalone version (STD) is intended for use in non-cluster
 * environments, like multi or uniprocessor isolated machines. Alignment matrix
 * subdivision parameters, 'hblk' and 'vblk', are set to 1 regardless of what
 * was specified on commandline. This means that, effectively, there are no
 * subdivisions made to the alignment matrix while calculating it, and all the
 * work is made in a single run. The other parallel-specific parameters, 'rank'
 * and 'size', are not used.
 *
 * The parallel version (MPI) is intended for use in cluster environments and
 * makes full use of blocking factors 'hblk' and 'vblk' to divide workload
 * between cluster nodes. The parameters 'rank' and 'size' are used by each
 * node to calculate destination nodes to send messages.
 */
blkalign_t blkalign_new (blkaligntype_t type, sequence_t s, sequence_t t, score_t scores, options_t options, int rank, int size) {
  blkalign_t aligner;
  char       psplit[PSPLIT_SIZEMAX];

  /* allocate space for one 'st_blkalign' element */
  aligner = (blkalign_t) malloc (sizeof(st_blkalign));

  /* set block aligner type */
  aligner->type = type;

  /* if running parallel block aligner, set number of parts  in which to split */
  /* alignment matrix, number of horizontal/vertical block subdivisions, node  */
  /* rank and communicator size */
  if (aligner->type == BLKALIGN_MPI) {
    aligner->split = (*options).split;
    aligner->hblk  = (*options).hblk;
    aligner->vblk  = (*options).vblk;
    aligner->rank  = rank;
    aligner->size  = size;
  }

  /* else, we're running standalone block aligner -- do not split alignment */
  /* matrix, use block subdivisions or node ranks and communicators         */
  else {
    aligner->split = 1;
    aligner->hblk  = 1;
    aligner->vblk  = 1;
    aligner->rank  = STANDALONE;
    aligner->size  = 1;
  }

  /* create and assign new block alignment strategy */
  aligner->strat = blkstrat_new ();

  /* initialize strategy variables */
  aligner->strat->s      = s;
  aligner->strat->t      = t;
  aligner->strat->slen   = strlen (s);
  aligner->strat->tlen   = strlen (t);
  aligner->strat->scores = scores;

  /* calculate frequently used aligner variables */
  /* horizontal length of each 'split' submatrix */
  aligner->split_len  = ceilf ((float)aligner->strat->tlen / (float)aligner->split);

  /* horizontal length of each processing node's submatrix */
  aligner->proc_len   = ceilf ((float)aligner->split_len   / (float)aligner->size);

  /* horizontal length of each 'hblk' subdivision (block width) */
  aligner->blk_width  = ceilf ((float)aligner->proc_len    / (float)aligner->hblk);

  /* vertical length of each 'vblk' subdivision (block height) */
  aligner->blk_height = ceilf ((float)aligner->strat->slen / (float)aligner->vblk);

  /* set 'split' print size */
  sprintf (psplit, "%d", aligner->split);
  aligner->psplit = strlen (psplit);

  return aligner;
}


/*
 * blkalign_postblk() is a handle called after processing the last block of
 * each vertical subdivision of the alignment matrix. It sends the output
 * column of the calculated block to the appropriate cluster node, usually the
 * next one. The only exception is the last cluster node, which sends its
 * output column to the root node instead.
 *
 * If cyclic block model is being used ('aligner->split > 1'), there is a
 * special case to be handled: if all nodes try to send output columns while
 * switching 'split' submatrix, a deadlock condition is created. To avoid this
 * problem, the node switching submatrices buffers his output column to be sent
 * later by blkalign_sendlock(), and continues execution.
 *
 * If using the standalone version of the block aligner algorithm this
 * function does nothing, as the matrix has already been fully calculated.
 */
void blkalign_postblk (blkalign_t aligner, count_t split_pass, count_t vblk_pass) {

  /* if running the parallel version of the algorithm... */
  if (aligner->type == BLKALIGN_MPI) {
    /* if not processing the final 'split' submatrix and reached the */
    /* last vertical block, buffer output column to avoid deadlock   */
    if ((vblk_pass  == (aligner->vblk  - 1)) &&
        (split_pass != (aligner->split - 1)))
      blkseq_copy (lockcol, aligner->outcol);

    else {
      /* all nodes but the last one send output column to the next node */
      if (aligner->rank != (aligner->size - 1)) {
        blkseq_send (aligner->outcol, aligner->rank + 1);
      }
      else {
        /* if not processing the final 'split' submatrix, last node sends */
        /* output column to the root node -- otherwise, do nothing        */
        if (split_pass != (aligner->split - 1))
          blkseq_send (aligner->outcol, ROOT_RANK);
      }
    }
  }

}


/*
 * blkalign_preblk() is a handle called before processing the first block of
 * each vertical subdivision of the alignment matrix. It initializes the input
 * column that will be used to calculate the block.
 *
 * It's worth noting that, if using the standalone version of the block aligner
 * algorithm, the column initialized by this function is the whole first column
 * of the matrix.
 */
void blkalign_preblk (blkalign_t aligner, count_t split_pass) {

  switch (aligner->rank) {
    case ROOT_RANK:
      /* if not processing the first 'split' submatrix, root */
      /* node receives input column from the last node       */
      if (split_pass != 0) {
        blkseq_recv (aligner->incol, aligner->size - 1);
        break;
      }
      /* FALLTHROUGH */
    case STANDALONE:
      /* else, initialize first column of the alignment matrix */
      /* -- std version initializes the whole fist column here */
      blkstrat_initcol (aligner->strat, aligner->incol);
      break;
    default:
      /* all other nodes receive input column from the previous node */
      blkseq_recv (aligner->incol, aligner->rank - 1);
  }

}


/*
 * blkalign_premx() is a handle called before processing the alignment matrix.
 * It initializes input rows that will be used to calculate the matrix.
 */
void blkalign_premx (blkalign_t aligner) {
  count_t j;

  /* iterate through all 'hblk' divisions, initializing rows */
  for (j = 0 ; j < aligner->hblk ; j++)
    blkstrat_initrow (aligner->strat, aligner->inrows[j]);

}


/*
 * blkalign_sendlock() is a handle called after releasing lock condition (by
 * calling blkalign_preblk(), previously) to send the buffered output column to
 * the appropriate node.
 */
void blkalign_sendlock (blkalign_t aligner) {

  /* all nodes but the last one send buffered column to the next node */
  if (aligner->rank != (aligner->size - 1))
    blkseq_send (lockcol, aligner->rank + 1);

  /* last node sends buffered column to the root node */
  else
    blkseq_send (lockcol, ROOT_RANK);

}


/*
 * blkalign_setcol() sets input/output column starting and ending positions.
 * The 'vblk_pass' parameter specifies which vertical block subdivision is
 * currently being processed -- the last one has special treatment.
 */
void blkalign_setcol (blkalign_t aligner, count_t vblk_pass) {
  
  /* set input column start */
  aligner->incol->start = (vblk_pass * aligner->blk_height) + 1;

  /* set input column end, watching out if this is the last vertical subdiv */
  if (vblk_pass != (aligner->vblk - 1))
    aligner->incol->end = (vblk_pass + 1) * aligner->blk_height;
  else
    aligner->incol->end = aligner->strat->slen;

  /* output column start/end values are the same, so copy them over */
  aligner->outcol->start = aligner->incol->start;
  aligner->outcol->end   = aligner->incol->end;

}


/*
 * blkalign_setrow() sets all input/output rows starting and ending positions
 * for a given 'split_pass' submatrix. All integer divisions are rounded up --
 * as implemented by the blkalign_new() function -- therefore special care has
 * to be taken so as not to overflow any given boundaries.
 */
void blkalign_setrow (blkalign_t aligner, count_t split_pass) {
  count_t    j;
  position_t end,
             proc_end,
             proc_start,
             split_end,
             split_start,
             start;

  /* calculate starting/ending positions of 'split' submatrix for this pass */
  split_start = (aligner->split_len * split_pass) + 1;
  split_end   = split_start + aligner->split_len - 1;

  /* each node calculates it's submatrix portion according to 'rank' */
  proc_start  = split_start + (aligner->proc_len * aligner->rank);
  proc_end    = proc_start + aligner->proc_len - 1;

  /* iterate through all 'hblk' divisions, setting start/end row positions */
  for (j = 0 ; j < aligner->hblk ; j++) {
    /* calculate this hblk start/end position */
    start = proc_start + ( j      * aligner->blk_width);
    end   = proc_start + ((j + 1) * aligner->blk_width) - 1;

    /* check if we didn't overflow this node's space boundaries */
    start = (start > proc_end) ? proc_end : start;
    end   = (end   > proc_end) ? proc_end : end;

    /* check if we didn't overflow 'split' submatrix space boundaries */
    start = (start > split_end) ? split_end : start;
    end   = (end   > split_end) ? split_end : end;

    /* and finally, check if we didn't overflow sequence boundaries */
    start = (start > (position_t)aligner->strat->tlen) ? aligner->strat->tlen : start;
    end   = (end   > (position_t)aligner->strat->tlen) ? aligner->strat->tlen : end;

    /* ok, now set this input/output row starting and ending position */
    aligner->inrows[j]->start  = start;
    aligner->outrows[j]->start = start;

    aligner->inrows[j]->end    = end;
    aligner->outrows[j]->end   = end;
  }

}

