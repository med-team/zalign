/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include "opt.h"


/* Macro definitions */

/*
 * CONVERT_BUF() is a simple macro that converts string parameter 'buf' to a
 * long integer, typecasts to a signed char and stores the result into 'score'.
 * We also check strtol() return value inside 'endptr' for invalid parameters.
 */
#define CONVERT_BUF(score,buf,endptr,flag)                 \
  score = (signed char) strtol (buf, &endptr, 10) * flag;  \
  if (endptr[0] != '\0') {                                 \
    printf ("error: invalid score parameter '%s'\n", buf); \
    goto PARSE_ERROR;                                      \
  }


/*
 * GET_BUF() is a simple macro that tokenizes input string 'src', using ","
 * (comma) string as delimiter. If no value is returned by strtok() we've
 * finished processing the score list, else we update 'count' and check if
 * the current 'buf' element is a negative value.
 */
#define GET_BUF(src,buf,count,flag)                        \
  buf = strtok (src, ",");                                 \
  if (buf == NULL)                                         \
    goto PARSE_STOP;                                       \
  count++;                                                 \
  NEGATIVE_BUF (buf, flag);


/*
 * NEGATIVE_BUF is a simple macro that checks whether input string 'buf'
 * represents a negative integer or not, setting 'flag' accordingly.
 */
#define NEGATIVE_BUF(buf,flag)                             \
  if (buf[0] == '-') {                                     \
    flag = -1;                                             \
    buf++;                                                 \
  }                                                        \
  else                                                     \
    flag = 1;


/* Function definitions */

/*
 * opt_exit() checks which program called opt_parse(), deciding the
 * appropriate 'progname' to pass over to opt_usage(). Decision is taken using
 * the 'rank' parameter, negative for the non-MPI versions.
 */
char opt_exit (int rank) {

  /* print usage information */
  opt_usage (opt_progname(rank));

  /* remind user of '-h' help option and exit */
  printf ("Try `%s -h' for more information.\n", opt_progname(rank));

  return EXIT;
}


/*
 * opt_panic() terminates the program, showing any pertinent system messages
 * through perror().
 */
char opt_panic (const char *message) {

  perror (message);

  return EXIT;
}


/*
 * opt_help() displays a help message to the user, showing available options and
 * explaining each one in detail. Program name is determined using the 'rank'
 * parameter.
 */
char opt_help (int rank) {

  /* print program name and version */
  printf ("%s %s %s\n", opt_progname(rank), VERSION, COPYRIGHT);

  /* print usage information */
  opt_usage  (opt_progname(rank));

  /* show and explain available program options */
  printf ("\nGeneral options:\n");
  printf (" -h           display this help and exit\n");
  printf ("\n");
  printf (" -s <scores>  specify a comma-separated list of scores to be used while\n");
  printf ("              calculating aligment matrices throughout the program. The list\n");
  printf ("              must be in the format \"-sMATCH,MISMATCH,GAP_OPEN,GAP_EXTENSION\"\n");
  printf ("              (without quotes), and is parsed in this PRECISE order; no spaces\n");
  printf ("              are allowed between values. If there are any unspecified\n");
  printf ("              parameters, these are set to default values and a warning message\n");
  printf ("              is issued; exceeding parameters are discarded\n");
  printf ("\nStage 2 options:\n");
  printf (" -S <split>   (mpialign only) number of parts in which to split the alignment\n");
  printf ("              matrix; after this step a cyclic block model is applied,\n");
  printf ("              subdividing each part equally between all available nodes\n");
  printf ("\n");
  printf (" -H <hblk>    (mpialign only) number of horizontal subdivisions made by each\n");
  printf ("              node to its alignment submatrix; since this value defines block\n");
  printf ("              width, a good choice should allow two full matrix lines to fit\n");
  printf ("              the processor's cache pages, improving algorithm performance\n");
  printf ("\n");
  printf (" -V <vblk>    (mpialign only) number of vertical subdivisions made by each node\n");
  printf ("              to its alignment submatrix; this value directly affects the\n");
  printf ("              amount of internode communication and is used ONLY if 'split' is\n");
  printf ("              set to 1, otherwise it is set to the number of available nodes\n");
  printf ("\n");

  return EXIT;
}


/*
 * opt_bcast() broadcasts relevant values of option structure 'options'
 * from the root node to all other nodes in MPI_COMM_WORLD through MPI
 * messaging.

 * Specifically, filename parameters 'options.sfile' and 'options.tfile' are
 * NOT relevant to program execution and too large to be transmitted.
 */
void opt_bcast (options_t options) {

  MPI_Bcast (&((*options).split), 1,                 MPI_INT,  ROOT_RANK, MPI_COMM_WORLD);
  MPI_Bcast (&((*options).hblk),  1,                 MPI_INT,  ROOT_RANK, MPI_COMM_WORLD);
  MPI_Bcast (&((*options).vblk),  1,                 MPI_INT,  ROOT_RANK, MPI_COMM_WORLD);
  MPI_Bcast ((*options).scores,   sizeof(st_scores), MPI_CHAR, ROOT_RANK, MPI_COMM_WORLD);

}


/*
 * opt_parse() parses the supplied 'argc' and 'argv' parameters, searching
 * for valid options passed to the program. If nonexistent options or wrong
 * number of file parameters are detected, exit.
 */
char opt_parse (int argc, char* argv[], int rank, st_options *options) {
  static int         c;

  extern char       *optarg;
  extern int         optind;

  /* check commandline arguments */
  while ((c = getopt(argc, argv, "hH:s:S:V:")) != -1) {
    switch (c) {
      /* show help message and exit */
      case 'h':
        return opt_help (rank);

      /* set number of horizontal subdivisions */
      case 'H':
        (*options).hblk   = atoi (optarg);
			  break;

      /* set custom score values, checking for errors while parsing list */
      case 's':
        if (((*options).scores = opt_scoreparse (optarg, rank)) == NULL)
          return EXIT;
			  break;

      /* set number of 'split' submatrices */
      case 'S':
        (*options).split  = atoi (optarg);
			  break;

      /* set number of vertical subdivisions */
      case 'V':
        (*options).vblk   = atoi (optarg);
        break;

      /* oops, no such parameter */
      default:
        return opt_exit (rank);
    }

    /* zero values are valid only for score options */
    if ((atoi(optarg) == 0) && (c != 's'))
      return opt_exit (rank);
  }

  /* check if exactly two file parameters were passed */
  argc -= optind;
  if (argc != 2)
    return opt_exit (rank);

  argv += optind;

  /* yep, now copy those filenames for later use */
  strcpy ((*options).sfile, argv[0]);
  strcpy ((*options).tfile, argv[1]);

  /* if no custom score values were passed, use default ones */
  if ((*options).scores == NULL)
    (*options).scores = score_new (DEFAULT_MATCH_SCORE, DEFAULT_MISMATCH_SCORE,
                                   DEFAULT_GAPOPEN_SCORE, DEFAULT_GAPEXTENSION_SCORE);

  return SUCCESS;
}


/*
 * opt_progname() evaluates parameter 'rank', deciding the appropriate program
 * name string to be returned.
 */
const char* opt_progname (int rank) {

  if (rank == ZALIGN_RANK)
    return ZALIGN;

  else
    return MPIALIGN;

}


/*
 * opt_scoreparse() parses score values 'list', returning an 'st_scores'
 * structure containing specified parameters. If any values are omitted, we use
 * default scores and emit a warning message to the user.
 *
 * All score values are checked -- if any parameter does not conform to the
 * algorithm's specification, we emit an error message and terminate the
 * program.
 */
score_t opt_scoreparse (char* list, int rank) {
  char        *buf,
              *endptr,
               list_count;
  scoreval_t   match,
               mismatch,
               gap_open,
               gap_extension;
  signed char  neg_flag;

  /* initialize list counter */
  list_count = 0;

  /* read MATCH value, checking for conformance */
  GET_BUF      (list, buf, list_count, neg_flag);
  CONVERT_BUF  (match, buf, endptr, neg_flag);

  if (match <= 0) {
    printf ("error: MATCH score must be a positive integer\n");
    goto PARSE_ERROR;
  }

  /* read MISMATCH value, checking for conformance */
  GET_BUF      (NULL, buf, list_count, neg_flag);
  CONVERT_BUF  (mismatch, buf, endptr, neg_flag);

  if (mismatch >= 0) {
    printf ("error: MISMATCH score must be a negative integer\n");
    goto PARSE_ERROR;
  }

  /* read GAP_OPEN value, checking for conformance */
  GET_BUF      (NULL, buf, list_count, neg_flag);
  CONVERT_BUF  (gap_open, buf, endptr, neg_flag);

  if (gap_open >= 0) {
    printf ("error: GAP_OPEN score must be a negative integer\n");
    goto PARSE_ERROR;
  }

  /* read GAP_EXTENSION value, checking for conformance */
  GET_BUF      (NULL, buf, list_count, neg_flag);
  CONVERT_BUF  (gap_extension, buf, endptr, neg_flag);

  if (gap_extension >= 0) {
    printf ("error: GAP_EXTENSION score must be a negative integer\n");
    goto PARSE_ERROR;
  }

  /* everything ok, keep going */
  goto PARSE_STOP;

/* something went wrong, emit error message and exit */
PARSE_ERROR:
  printf ("Please re-run `%s', specifying appropriate score parameters.\n", opt_progname(rank));
  return NULL;

/* parsing successful, set scores and warn if any default values are used */
PARSE_STOP:
  switch (list_count) {
    case 0:
      match         = DEFAULT_MATCH_SCORE;
      printf ("\n(warning) using default MATCH value         [%d]", DEFAULT_MATCH_SCORE);
      /* FALLTHROUGH */
    case 1:
      mismatch      = DEFAULT_MISMATCH_SCORE;
      printf ("\n(warning) using default MISMATCH value      [%d]", DEFAULT_MISMATCH_SCORE);
      /* FALLTHROUGH */
    case 2:
      gap_open      = DEFAULT_GAPOPEN_SCORE;
      printf ("\n(warning) using default GAP_OPEN value      [%d]", DEFAULT_GAPOPEN_SCORE);
      /* FALLTHROUGH */
    case 3:
      gap_extension = DEFAULT_GAPEXTENSION_SCORE;
      printf ("\n(warning) using default GAP_EXTENSION value [%d]\n", DEFAULT_GAPEXTENSION_SCORE);
      /* FALLTHROUGH */
    case 4:
      break;

    /* gee, if we reach this point something really weird happened */
    default:
      return NULL;
  }

  return score_new (match, mismatch, gap_open, gap_extension);
}


/*
 * opt_usage() simply prints a helpful message to the user, showing how to use
 * the program identified by 'progname'.
 */
void opt_usage (const char *progname) {

  printf ("usage: %s [-s scores] [-S split] [-H hblk] [-V vblk] file1 file2\n", progname);

}

