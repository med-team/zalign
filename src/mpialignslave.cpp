/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include <stdlib.h>
#include <iostream>

#include "alignbounds.h"
#include "alignment.h"
#include "linearfickettaligner.h"
#include "mpialign.h"
#include "mpialignslave.h"
#include "score.h"
#include "sequence.h"


extern int comm_rank;
extern sequence_t s;
extern sequence_t t;
extern score_t sm;

using namespace std;


int receiveCommandFrom(int from) {
  int command;
  MPI_Status status;
  
  MPI_Recv(&command, 1, MPI_UNSIGNED, from, 0, MPI_COMM_WORLD, &status);

  return command;
}


void receiveBounds (alignbounds_t bounds) {

  alignbounds_recv (bounds, ROOT_RANK);
}


void sendAlignment (alignment_t alignment, size_t size) {

  alignment_send (alignment, ROOT_RANK, TAG_ALIGNMENT_ANSWER);
}


void sendJobRequest() {
  char dummy;
  
  MPI_Send(&dummy, 1, MPI_CHAR, 0, TAG_JOB_REQUEST, MPI_COMM_WORLD);
}

/**
 * @brief Escravo da 2a fase do alinhador
 */
void slave() {
  int command;
  bool terminate;
  
  terminate = false;
  
  while (!terminate) {
    alignbounds_t bounds;
    alignment_t alignment;
    size_t size;

    sendJobRequest();
    command = receiveCommandFrom(0);
    
    switch (command) {
      case COMMAND_ALIGN:
        bounds = alignbounds_new ();

        receiveBounds (bounds);
        alignment = align(bounds, &size);
        sendAlignment (alignment, size);

        alignbounds_free (bounds);
        alignment_free (alignment);
        break;
        
      case COMMAND_TERMINATE:
        terminate = true;
        break;
    }
  }
  
}

