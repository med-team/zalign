/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#include "alignment.h"


/* Macro definitions */

#define MAX2(a,b)     ((a > b) ? a : b)
#define MAX4(a,b,c,d) (MAX2 (MAX2(a,b), MAX2(c,d)))


/*
 * DISPLAY_SEGMENT() is a simple macro that diplays parameter 'seg' from 'start'
 * to 'end' positions.
 */
#define DISPLAY_SEGMENT(seg,start,end,i) \
  for (i = start ; i <= end ; i++)       \
    printf ("%c", seg[i]);


/* Function definitions */

/*
 * alignment_add() adds 'step' to 'alignment's path list, updating counters
 * and pointers as needed. If the path list runs out of allocated memory, it is
 * grown by PATH_ALLOC bytes.
 */
void alignment_add (alignment_t alignment, step_t step) {

  /* increment alignment length */
  alignment->len++;

  /* write 'step' to our path list */
  *(alignment->cur) = step;

  /* decrement starting positions, depending on 'step' */
  switch (step) {
    case PATH_UP:
      alignment->s_start--;
      break;

    case PATH_DIAG:
      alignment->s_start--;
      alignment->t_start--;
      break;

    case PATH_LEFT:
      alignment->t_start--;
      break;
  }

  /* check if we've exausted memory space of our alignment, grow if needed */
  if (alignment->len == ((position_t) alignment->path_size)) {
    alignment->path_size += PATH_ALLOC;
    alignment->path = (step_t*) realloc (alignment->path, alignment->path_size
                                         * sizeof(step_t));
  }

  /* update current position pointer */
  alignment->cur++;

}


/*
 * alignment_display() displays 'alignment' on screen, in a format very similar
 * to BLAST's output. Information printed includes: alignment score, starting
 * and ending positions, identity and gap percentages, and pertinent chunks of
 * aligned sequences with corresponding alignment data.
 */
void alignment_display (alignment_t alignment, sequence_t s, sequence_t t) {
  char          space[SPACE_SIZEMAX],
               *s_line,
               *a_line,
               *t_line;
  int           display_width,
                space_size;
  position_t    match,
                mismatch,
                hgap,
                vgap,
                tgap,
                s_end,
                t_end,
               *s_pos,
               *t_pos,
                i,
                j;
  step_t        step;

  /* set console display width */
  display_width = 60;

  /* alignment ending positions */
  s_end = alignment->s_end - 1;
  t_end = alignment->t_end - 1;

  /* arrays holding sequence and alignment data */
  s_line = (char*) malloc ((alignment->len + 1) * sizeof(char));
  a_line = (char*) malloc ((alignment->len + 1) * sizeof(char));
  t_line = (char*) malloc ((alignment->len + 1) * sizeof(char));

  /* arrays holding positioning data */
  s_pos = (position_t*) malloc ((alignment->len + 1) * sizeof(position_t));
  t_pos = (position_t*) malloc ((alignment->len + 1) * sizeof(position_t));

  /* intialize counters */
  match    = 0;
  mismatch = 0;
  hgap     = 0;
  vgap     = 0;

  /* iterate through alignment length, filling in arrays */
  for (i = 1 ; i <= alignment->len ; i++) {
    /* get current traceback step */
    step = alignment->path[i - 1];

    switch(step) {
      /* upward step -- S element aligned with T gap */
      case PATH_UP:
        s_line[alignment->len - i] = s[s_end];
        a_line[alignment->len - i] = ' ';
        t_line[alignment->len - i] = '-';

        s_pos[alignment->len - i] = s_end + 1;
        t_pos[alignment->len - i] = t_end + 1;

        s_end--;
        vgap++;
        break;

      /* leftward step -- S gap aligned with T element */
      case PATH_LEFT:
        s_line[alignment->len - i] = '-';
        a_line[alignment->len - i] = ' ';
        t_line[alignment->len - i] = t[t_end];

        s_pos[alignment->len - i] = s_end + 1;
        t_pos[alignment->len - i] = t_end + 1;

        t_end--;
        hgap++;
        break;

      /* diagonal step -- S element aligned with T element */
      case PATH_DIAG:
        s_line[alignment->len - i] = s[s_end];
        t_line[alignment->len - i] = t[t_end];

        /* check if it's a match or mismatch */
        if (s[s_end] == t[t_end]) {
          a_line[alignment->len - i] = '|';
          match++;
        }
        else {
          a_line[alignment->len - i] = '.';
          mismatch++;
        }

        s_pos[alignment->len - i] = s_end + 1;
        t_pos[alignment->len - i] = t_end + 1;

        s_end--;
        t_end--;
        break;
    }
  }

  /* calculate total amount of gaps */
  tgap = hgap + vgap;

  /* print summary information about this alignment */
  printf ("  Score = %d,", alignment->score);
  printf (" Start = (%ld,%ld)", alignment->s_start, alignment->t_start);
  printf (" End = (%ld,%ld)\n", alignment->s_end, alignment->t_end);
  printf ("  Identities = %ld/%ld (%ld%%),", match, alignment->len,
          (match * 100)/alignment->len);
  printf (" Gaps = %ld/%ld (%ld%%)\n", tgap, alignment->len,
          (tgap * 100)/alignment->len);

  /* assign 'space_size' with the amount of spacing to be used */
  sprintf (space, "%ld", MAX4(alignment->s_start, alignment->s_end,
                              alignment->t_start, alignment->t_end));
  space_size = strlen (space);

  /* fill 'space' with 'space_size' space characters, and NUL-terminate it */
  for (i = 0; i < space_size; i++)
    space[i] = ' ';
  space[space_size] = '\0';

  i = 0;

  /* if we haven't reached alignment length... */
  while (i < alignment->len) {
    /* set display width */
    display_width = ((alignment->len - i) < display_width) ?
                     (alignment->len - i) : display_width;

    /* print query sequence segment */
    printf ("  Query %-*ld ", space_size, s_pos[i]);
    DISPLAY_SEGMENT (s_line, i, i + display_width - 1, j);
    printf (" %-*ld\n", space_size, s_pos[i + display_width - 1]);

    /* print corresponding alignment data segment */
    printf ("        %s ", space);
    DISPLAY_SEGMENT (a_line, i, i + display_width - 1, j);
    printf ("  \n");

    /* print subject sequence segment */
    printf ("  Sbjct %-*ld ", space_size, t_pos[i]);
    DISPLAY_SEGMENT (t_line, i, i + display_width - 1, j);
    printf (" %-*ld\n\n", space_size, t_pos[i + display_width - 1]);

    /* update our counter */
    i += display_width;
  }

  /* housekeeping... */
  free(s_line);
  free(a_line);
  free(t_line);
  free(s_pos);
  free(t_pos);

}


/*
 * alignment_free simply deallocates memory space occupied by 'alignment' and
 * its alignment path list 'alignment->path'.
 */
void alignment_free (alignment_t alignment) {

  /* deallocate alignment path list */
  free (alignment->path);

  /* and alignment itself */
  free (alignment);

}


/*
 * alignment_new() creates a new 'st_alignment' element, 'alignment',
 * initializes variables, allocates PATH_ALLOC bytes to its alignment path list
 * and returns.
 */
alignment_t alignment_new (sequence_t s, sequence_t t) {
  alignment_t alignment;

  /* allocate space for one 'st_alignment' element */
  alignment = (alignment_t) malloc (sizeof(st_alignment));

  /* initialize path list length */
  alignment->len    = 0;

  /* keep track of path list size */
  alignment->path_size = PATH_ALLOC;

  /* allocate PATH_ALLOC space for our alignment path list */
  alignment->path = (step_t*) malloc (alignment->path_size * sizeof(step_t));
  alignment->cur  = alignment->path;

  return alignment;
}


/*
 * alignment_recv() receives an 'st_alignment' structure into 'alignment' from
 * any source through MPI messages taggeg with 'tag'.
 *
 * Note that 'alignment' must be preallocated before calling this function.
 * Also note that the alignment path list received is from the same node that
 * sent the alignment itself, initially.
 */
void alignment_recv (alignment_t alignment, int tag) {
  MPI_Status status;

  /* receive alignment structure */
  MPI_Recv (alignment, sizeof(st_alignment), MPI_CHAR, MPI_ANY_SOURCE, tag,
            MPI_COMM_WORLD, &status);

  /* allocate appropriate size to path list */
  alignment->path = (step_t*) malloc (alignment->path_size * sizeof(step_t));

  /* receive alignment path list */
  MPI_Recv (alignment->path, alignment->path_size, MPI_CHAR, status.MPI_SOURCE,
            tag, MPI_COMM_WORLD, &status);

}


/*
 * alignment_send() sends 'st_alignment' structure 'alignment' and its
 * alignment path list to destination node 'dest', through MPI messages tagged
 * with 'tag'.
 */
void alignment_send (alignment_t alignment, int dest, int tag) {

  /* send alignment structure */
  MPI_Send (alignment,       sizeof(st_alignment), MPI_CHAR, dest, tag,
            MPI_COMM_WORLD);

  /* and alignment path list */
  MPI_Send (alignment->path, alignment->path_size, MPI_CHAR, dest, tag,
            MPI_COMM_WORLD);

}

