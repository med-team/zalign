#!/bin/sh
# Run this script to setup the source tree
# Afterwards, run './configure && make' as usual

aclocal -I ./m4
autoheader
autoconf
libtoolize --automake -c
automake -a -c
echo "\n>> Now run './configure && make' to compile zAlign\n"
