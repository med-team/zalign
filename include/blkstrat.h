/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef BLKSTRAT_H
#define BLKSTRAT_H

#include <stdlib.h>

#include "alignarray.h"
#include "alignbounds.h"
#include "blkborder.h"
#include "score.h"
#include "sequence.h"


/* Type definitions */

struct s_blkstrat {
  alignarray_t   alignarray;          /* array holding the best alignments */
  blkborder_t    inborder,            /* block input border */
                 outborder;           /* block output border */
  score_t        scores;              /* scoring values to be assigned */
  sequence_t     s,                   /* S sequence */
                 t;                   /* T sequence */
  size_t         slen,                /* S sequence length */
                 tlen;                /* T sequence length */
};
typedef struct s_blkstrat  st_blkstrat;
typedef struct s_blkstrat* blkstrat_t;


/* Function definitions */

void       blkstrat_align   (blkstrat_t);
void       blkstrat_free    (blkstrat_t);
void       blkstrat_initcol (blkstrat_t, blkseq_t);
void       blkstrat_initrow (blkstrat_t, blkseq_t);
blkstrat_t blkstrat_new     (void);

#endif

