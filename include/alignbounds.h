/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef ALIGNBOUNDS_H
#define ALIGNBOUNDS_H

#include <mpi.h>
#include <stdlib.h>

#include "types.h"


/* Type definitions */

struct s_alignbounds {
  struct s_alignbounds *next;    /* pointer to next alignment in 'alignarray' */
  position_t            s_start, /* position in S in which alignment starts */
                        s_end,   /* position in S in which alignment ends */
                        t_start, /* position in T in which alignment starts */
                        t_end,   /* position in T in which alignment ends */
                        hdiv,    /* worst inferior diagonal, relative to alignment start */
                        vdiv;    /* worst superior diagonal, relative to alignment start */
  simentry_t            score;   /* alignment score */
};
typedef struct s_alignbounds  st_alignbounds;
typedef struct s_alignbounds* alignbounds_t;


/* Function definitions */

void          alignbounds_free (alignbounds_t);
alignbounds_t alignbounds_new  (void);
void          alignbounds_recv (alignbounds_t, int);
void          alignbounds_send (alignbounds_t, int);

#endif

