/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef ALIGNARRAY_H
#define ALIGNARRAY_H

#include <stdlib.h>

#include "alignbounds.h"
#include "types.h"


/* Type definitions */

struct s_alignarray {
  count_t       count;      /* number of elements in alignment array */
  alignbounds_t alignments; /* linked list of elements in alignment array */
  simentry_t    best_score; /* score of all alignments in aligment array */
};
typedef struct s_alignarray  st_alignarray;
typedef struct s_alignarray* alignarray_t;


/* Function definitions */

void         alignarray_add   (alignarray_t, alignbounds_t);
void         alignarray_clear (alignarray_t);
void         alignarray_free  (alignarray_t);
alignarray_t alignarray_new   (void);

#endif

