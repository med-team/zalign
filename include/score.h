/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef SCORE_H
#define SCORE_H

#include <stdlib.h>


/* Type definitions */

typedef signed char scoreval_t;  /* type assigned to score values */

struct s_scores {
  scoreval_t match,              /* score to assign if comparison is a 'match' */
             mismatch,           /* penalty for a 'mismatch' comparison */
             gap_open,           /* penalty for a gap-open */
             gap_extension;      /* penalty for each gap-extension */
};
typedef struct s_scores  st_scores;
typedef struct s_scores* score_t;


/* Function definitions */

void    score_free  (score_t);
score_t score_new   (scoreval_t, scoreval_t, scoreval_t, scoreval_t);

#endif

