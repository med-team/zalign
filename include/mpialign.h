/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef MPIALIGN_H
#define MPIALIGN_H

/** @file mpialign.h
 * @brief Define constantes e fun��es utilizadas por mpialign
 */

#include "alignbounds.h"

/// Alinha duas seq��ncias
#define COMMAND_ALIGN         1

/// Termina o loop infinito do escravo
#define COMMAND_TERMINATE     2

/// Requisi��o de trabalho
#define TAG_JOB_REQUEST       1

/// Resposta de alinhamento
#define TAG_ALIGNMENT_ANSWER  2


alignment_t align (alignbounds_t bounds, size_t * size);

#endif

