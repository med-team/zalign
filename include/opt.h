/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef OPT_H
#define OPT_H

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "score.h"
#include "types.h"

#define VERSION   "0.9.1"                     /* program version */
#define COPYRIGHT "(C) 2006-2009 zAlign Team" /* program copyright notice */

#define SUCCESS                     0         /* code returned for successful operation */
#define EXIT                        1         /* code returned for program termination */

#define MPIALIGN                   "mpialign" /* 'mpialign' executable program name */
#define ZALIGN                     "zalign"   /* 'zalign' executable program name */

#define ZALIGN_RANK                -1         /* 'zalign' program rank */

#define DEFAULT_MATCH_SCORE         1         /* default MATCH score value */
#define DEFAULT_MISMATCH_SCORE     -3         /* default MISMATCH score value */
#define DEFAULT_GAPOPEN_SCORE      -5         /* default GAP_OPEN score value */
#define DEFAULT_GAPEXTENSION_SCORE -2         /* default GAP_EXTENSION score value */


/* Type definitions */

struct s_options {
  char    sfile[FILENAME_MAX],                /* S sequence filename */
          tfile[FILENAME_MAX];                /* T sequence filename */
  count_t split,                              /* parts in which to split the alignment matrix */
          hblk,                               /* number of horizontal subdivisions made to alignment submatrix */
          vblk;                               /* number of vertical subdivisions made to alignment submatrix */
  score_t scores;                             /* score values to be used while calculating alignment matrices */
};
typedef struct s_options  st_options;
typedef struct s_options* options_t;


/* Function definitions */

char        opt_exit       (int);
char        opt_panic      (const char*);
char        opt_help       (int);
void        opt_bcast      (options_t);
char        opt_parse      (int, char*[], int, st_options*);
const char* opt_progname   (int);
score_t     opt_scoreparse (char*, int);
void        opt_usage      (const char*);

#endif

