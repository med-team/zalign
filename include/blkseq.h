/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef BLKSEQ_H
#define BLKSEQ_H

#include <mpi.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"


/* Type definitions */

struct s_blkseq {
  position_t  start, /* sequence starting position */
              end;   /* sequence ending position */
  simentry_t *s;     /* similarity values array */
  simentry_t *p;     /* array holding gap values inserted in sequence 't' */
  simentry_t *q;     /* array holding gap values inserted in sequence 's' */
  position_t *hdiv;  /* maximum horizontal (i.e. inferior diagonal) divergence */
  position_t *vdiv;  /* maximum vertical (i.e. superior diagonal) divergence */
};
typedef struct s_blkseq  st_blkseq;
typedef struct s_blkseq* blkseq_t;


/* Function definitions */

void     blkseq_copy (blkseq_t, blkseq_t);
void     blkseq_free (blkseq_t);
blkseq_t blkseq_new  (position_t, position_t);
void     blkseq_recv (blkseq_t, int);
void     blkseq_send (blkseq_t, int);

#endif

