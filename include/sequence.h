/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <mpi.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"


/* Function definitions */

sequence_t sequence_bcast  (sequence_t, int);
sequence_t sequence_invert (sequence_t);
sequence_t sequence_subseq (sequence_t, position_t, position_t);

#endif

