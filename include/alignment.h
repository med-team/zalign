/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef ALIGNMENT_H
#define ALIGNMENT_H

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"

#define PATH_ALLOC    1024 * 1024 /* increase path list allocation by 1M-sized blocks */

#define PATH_UP        0          /* upward step -- S element aligned with T gap */
#define PATH_DIAG      1          /* diagonal step -- S element aligned with T element */
#define PATH_LEFT      2          /* leftward step -- S gap aligned with T element */

#define SPACE_SIZEMAX 11          /* maximum amount of leading spaces to 'a_line' */


/* Macro definitions */

/*
 * SET_DISPLACEMENT() is a simple macro that applies displacement 'disp' to
 * 'start' and 'end' positions.
 */
#define SET_DISPLACEMENT(start,end,disp) \
  start += disp;                         \
  end   += disp;


/* Type definitions */

typedef unsigned char step_t;     /* alignment step type */

struct s_alignment {
    step_t     *path,             /* alignment path list, i.e the alignment itself */
               *cur;              /* current path list element */
    size_t      path_size;        /* alignment path list size */
    position_t  len,              /* alignment length */
                s_start,          /* position in S in which alignment starts */
                s_end,            /* position in S in which alignment ends */
                t_start,          /* position in T in which alignment starts */
                t_end;            /* position in T in which alignment ends */
    simentry_t  score;            /* alignment score */
};
typedef struct s_alignment  st_alignment;
typedef struct s_alignment* alignment_t;


/* Function definitions */

void        alignment_add     (alignment_t, step_t);
void        alignment_display (alignment_t, sequence_t, sequence_t);
void        alignment_free    (alignment_t);
alignment_t alignment_new     (sequence_t, sequence_t);
void        alignment_recv    (alignment_t, int);
void        alignment_send    (alignment_t, int, int);

#endif

