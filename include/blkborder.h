/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef BLKBORDER_H
#define BLKBORDER_H

#include "blkseq.h"

#define INPUT_BORDER  1             /* input borders -- top and left */
#define OUTPUT_BORDER 2             /* output borders -- bottom and right */


/* Type definitions */

typedef unsigned char bordertype_t; /* block border available types */

struct s_blkborder {
  bordertype_t type;                /* block border type */
  blkseq_t     column,              /* block sequence holding left/right columns */
               row;                 /* block sequence holding top/bottom rows */
};
typedef struct s_blkborder  st_blkborder;
typedef struct s_blkborder* blkborder_t;


/* Function definitions */

void        blkborder_free (blkborder_t);
blkborder_t blkborder_new  (bordertype_t);

#endif

