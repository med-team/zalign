/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef ZALIGN_H
#define ZALIGN_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "alignbounds.h"
#include "alignment.h"
#include "blkalign.h"
#include "fastareader.h"
#include "linearfickettaligner.h"
#include "opt.h"
#include "score.h"
#include "sequence.h"
#include "types.h"


/* Function definitions */

void zalign_stage1 (sequence_t, sequence_t, score_t);
void zalign_stage2 (alignbounds_t, sequence_t, sequence_t, score_t);

#endif

