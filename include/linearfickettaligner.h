/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef LINEARFICKETTALIGNER_H
#define LINEARFICKETTALIGNER_H

#include <assert.h>
#include <stdlib.h>
#include <vector>

#include "alignment.h"
#include "limitsplotter.h"
#include "score.h"

using namespace std;

class LinearFickettAligner : public LimitsPlotter {

  template <typename T> class LinearFickettTable {
    public:
      LinearFickettTable(position_t k, position_t l) {
        mWidth = (k - l) + 1;
        m_LinesPerExtension = 128;
        m_LinesCount = 0;
      }
    
      ~LinearFickettTable() {
        count_t i;
      
        for (i = 0; i < mLines.size(); i ++) {
          if (mLines[i] != NULL)
            freeSegment(i);

          if (mCache[i] != NULL)
            freeCache(i);
        }
      }

      bool isAllocated(count_t line) { return mLines[line / m_LinesPerExtension] != NULL; }

      size_t getAllocatedBytes() { return mWidth * m_LinesPerExtension * sizeof(T) * m_LinesCount; }

      void setLinesPerExtension(const int& theValue) { m_LinesPerExtension = theValue; }
  
      int getLinesPerExtension() const { return m_LinesPerExtension; }
    
      T *operator[](size_t index) {
        return ((T*)mLines[index / m_LinesPerExtension]) 
            + (mWidth * (index % m_LinesPerExtension));
      }
    
      void reserve(count_t line) {
        while (line >= (mLines.size() * m_LinesPerExtension))
          extendTable();
      }
      
      /* @bug Isso aqui est� errado. A Linha de cache tem que ser a mais alta quando esbarrar no vazio */
      void freeTop(count_t line) {
        signed int segment;
        
        segment = (line / m_LinesPerExtension) - 1;
       
        for (; segment >= 0; segment --) {
          if (segment == 0)
            break;

          if (!isAllocated((segment - 1)  * m_LinesPerExtension))
            break;

          freeSegment(segment);
        }
        
        if (segment >= 0)
          segmentToCache(segment);
      }
      
      void freeBottom(count_t line) {
        signed int segment;
        
        for (segment = (line / m_LinesPerExtension) + 1 ; (segment < m_LinesCount) && (isAllocated(segment * m_LinesPerExtension)) ; segment++)
          freeSegment(segment);
      }
      
      position_t raiseCache(count_t line) {
        signed int segment;

        for (segment = line / m_LinesPerExtension ; ((segment > 0) && (!isCached(segment))) ; segment--)
          allocateSegment(segment);
        
        assert(segment >= 0);
        assert(isCached(segment));
        
        cacheToSegment(segment);
        return segment * m_LinesPerExtension;
      }
    
    private:
      position_t mWidth;
      std::vector<T*> mLines;
      std::vector<T*> mCache;
      int m_LinesPerExtension;
      int m_LinesCount;
    
      bool isCached(count_t segment) { return mCache[segment] != NULL; }
      
      void extendTable() {
        size_t size;
      
        size = mWidth * m_LinesPerExtension * sizeof(T);
        mLines.push_back(NULL);
        mCache.push_back(NULL);
        allocateSegment(m_LinesCount);
        m_LinesCount++;
      }
      
      void segmentToCache(count_t segment) {
        size_t size;

        assert (mLines[segment] != NULL);
        assert (mCache[segment] == NULL);
              
        size = mWidth * sizeof(T);
        allocateCache(segment);
        
        memcpy(mCache[segment], mLines[segment], size);
        freeSegment(segment);
      }
      
      void cacheToSegment(count_t segment) {
        size_t size;

        assert (mLines[segment] == NULL);
        assert (mCache[segment] != NULL);
        
        allocateSegment(segment);
        
        size = mWidth * sizeof(T);
        memcpy(mLines[segment], mCache[segment], size);
        freeCache(segment);
      }
      
      void allocateSegment(count_t segment) {
        size_t size;
        
        assert (mLines[segment] == NULL);
        
        size = mWidth * m_LinesPerExtension * sizeof(T);
        mLines[segment] = (T *)malloc(size);
        memset(mLines[segment], 0x00, size);
      }
      
      void allocateCache(count_t segment) {
        size_t size;
        
        assert (mCache[segment] == NULL);
        
        size = mWidth * sizeof(T);
        mCache[segment] = (T *)malloc(size);
        memset(mCache[segment], 0x00, size);
      }
      
      void freeSegment(count_t segment) {
        assert (mLines[segment] != NULL);
        
        free(mLines[segment]);
        mLines[segment] = NULL;
      }
      
      void freeCache(count_t segment) {
        assert (mCache[segment] != NULL);
        
        free(mCache[segment]);
        mCache[segment] = NULL;
      }
  };
  
  typedef unsigned char tracebackholder_t;
    
  public:

     LinearFickettAligner(sequence_t  s, sequence_t  t, score_t sm, simentry_t D);
    ~LinearFickettAligner();

    alignment_t align();
    
    alignment_t getAlignment(count_t index);
    
    float getProcessed();

    size_t getAllocatedBytes() { return mAllocatedBytes; }
    
    /* superior diagonal that limits processing */
    void setL(const position_t& theValue) { mL = theValue; }
    position_t getL() const { return mL; }

    /* inferior diagonal that limits processing */
    void setK(const position_t& theValue) { mK = theValue; }
    position_t getK() const { return mK; }
    
    void setFindOnlyFirst(bool theValue) { mFindOnlyFirst = theValue; }
    bool getFindOnlyFirst() const { return mFindOnlyFirst; }

    count_t count() { return mCount; }
    sequence_t getS() const { return s; }
    sequence_t getT() const { return t; }
    score_t getScoringMatrix() const { return sm; }
  
  private:
    /* methods */
    alignment_t traceback(position_t i, position_t j);
    void initializeFickettTable();
    bool processLines(position_t startline, position_t endline);

    /* from aligner */
    sequence_t s;
    sequence_t t;
    score_t sm;
    count_t mCount;

    /* from fickettaligner */
    simentry_t *mSim;   // Matriz de similaridades.
    simentry_t mD;      // Valor de estimativa do score do alinhamento.
    count_t mProcessed; // N�mero de c�lulas efetivamente processadas para obter o alinhamento.

    /* already here in linearfickettaligner */
    LinearFickettTable<simentry_t> *tSim;    /// Matriz de valores de similaridades
    LinearFickettTable<simentry_t> *tP;    /// Matriz de valores para gaps inseridos em t.
    LinearFickettTable<simentry_t> *tQ;    /// Matriz de valores para gaps inseridos em s.
    LinearFickettTable<tracebackholder_t> *tTraceback;    /// Matriz de traceback
    
    std::vector<alignment_t> mBestAlignmentVector;

    position_t mK;
    position_t mL;
    
    bool mFindOnlyFirst;
    
    size_t mAllocatedBytes;
    
    bool alignmentFound;
    bool writePlot;
    FILE *file;
    scoreval_t match;
    scoreval_t mismatch;
    scoreval_t gap_open;
    scoreval_t gap_extension;
    position_t slen;
    position_t tlen;
    sequence_t seqs;
    sequence_t seqt;
    
    position_t m_sBestScore;
    position_t m_tBestScore;

    size_t m_BytesPerLine;
    size_t m_BlockMemoryLimit;
    
};

#endif
