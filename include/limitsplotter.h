/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef LIMITSPLOTTER_H
#define LIMITSPLOTTER_H
//#define FILENAME_MAX 100

/**" @file limitsplotter.h
 * @brief Define a interface LimitsPlotter.
 */

#include <string.h>
#include <stdio.h>

#include "types.h"

/**
 * @brief Define uma interface para objetos que gravam informa��es de limite de
 * processamento.
 * 
 * @author Rodolfo B. Batista
 */
class LimitsPlotter{
  public:

/**
* @brief Define o nome do arquivo onde ser�o gravadas as coordenadas dos
* limites de processamento.
* Os dados gravados podem ser lidos com o Gnuplot.
* 
* @param theValue Nome do arquivo onde ser�o gravadas as coordenadas de 
* limites.
*/
void setLimitsPlotFilename(const char *theValue)
{
  strcpy(mLimitsPlotFilename, theValue);
}
  

/**
* @brief Obt�m o nome do arquivo no qual ser�o gravadas as coordenadas dos
* limites de processamento.
* 
* @return Nome do arquivo onde ser�o gravadas as coordenadas de limites.
* @sa setSDisplacement, setTDisplacement
*/
const char *getLimitsPlotFilename() const
{
  return mLimitsPlotFilename;
}

/**
* @brief Define qual o deslocamento a ser aplicado aos dados gravados, em S.
* @param theValue Deslocamento a ser aplicado aos dados gravados.
*/
void setSDisplacement(const position_t theValue)
{
  mSDisplacement = theValue;
}
  

/**
* @brief Obt�m o deslocamento a ser aplicado aos dados gravados, em S.
* @return Deslocamento a ser aplicado aos dados gravados.
*/
position_t getSDisplacement() const
{
  return mSDisplacement;
}

/**
* @brief Define qual o deslocamento a ser aplicado aos dados gravados, em T.
* @param theValue Deslocamento a ser aplicado aos dados gravados.
*/
void setTDisplacement(const position_t theValue)
{
  mTDisplacement = theValue;
}
  

/**
* @brief Obt�m o deslocamento a ser aplicado aos dados gravados, em T.
* @return Deslocamento a ser aplicado aos dados gravados.
*/
position_t getTDisplacement() const
{
  return mTDisplacement;
}
  
  protected:
    /// Nome do DataFile onde ser�o gravados os dados dos limites do 
    /// processamento.
    char mLimitsPlotFilename[FILENAME_MAX];
    
    /// Deslocamento, em S, do inicio da matriz de similaridades.
    position_t mSDisplacement;
    
    /// Deslocamento, em T, do inicio da matriz de similaridades.
    position_t mTDisplacement;

};

#endif
