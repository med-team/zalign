/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef BLKALIGN_H
#define BLKALIGN_H

#include <math.h>
#include <stdlib.h>

#include "blkseq.h"
#include "blkstrat.h"
#include "opt.h"
#include "types.h"

#define BLKALIGN_STD    1             /* standalone (single-processor) block aligner */
#define BLKALIGN_MPI    2             /* parallel block aligner */

#define STANDALONE     -1             /* standalone node rank */

#define START           0             /* block aligner message -- start */
#define SPLIT           1             /* block aligner message -- starting 'split' submatrix */
#define BLOCK           2             /* block aligner message -- finishing one 'hblk' */
#define END             3             /* block aligner message -- end */

#define PSPLIT_SIZEMAX 11             /* maximum 'split' print size */


/* Type definitions */

typedef unsigned char blkaligntype_t; /* block aligner available types */

struct s_blkalign {
  blkaligntype_t  type;               /* block aligner type */
  blkseq_t       *inrows,             /* block input rows */
                 *outrows,            /* block output rows */
                  incol,              /* block input column */
                  outcol;             /* block output column */
  blkstrat_t      strat;              /* alignment strategy */
  count_t         split,              /* parts in which to split alignment matrix */
                  hblk,               /* number of horizontal divisions to subalignment matrix */
                  vblk;               /* number of vertical divisions to subalignment matrix */
  position_t      split_len,          /* horizontal length of each 'split' submatrix */
                  proc_len,           /* horizontal length of each processing node's submatrix */
                  blk_width,          /* horizontal length of each 'hblk' subdivision (block width) */
                  blk_height;         /* vertical length of each 'vblk' subdivision (block height) */
  int             psplit,             /* 'split' print size, i.e. string lenght of 'split' */
                  rank,               /* node rank -- BLKALIGN_MPI only */
                  size;               /* COMM_WORLD communicator size -- BLKALIGN_MPI only */
};
typedef struct s_blkalign  st_blkalign;
typedef struct s_blkalign* blkalign_t;


/* Function definitions */

void       blkalign_align        (blkalign_t);
void       blkalign_allocborders (blkalign_t);
void       blkalign_free         (blkalign_t);
void       blkalign_msg          (blkalign_t, char, int, int, int);
blkalign_t blkalign_new          (blkaligntype_t, sequence_t, sequence_t, score_t, options_t, int, int);
void       blkalign_postblk      (blkalign_t, count_t, count_t);
void       blkalign_preblk       (blkalign_t, count_t);
void       blkalign_premx        (blkalign_t);
void       blkalign_sendlock     (blkalign_t);
void       blkalign_setcol       (blkalign_t, count_t);
void       blkalign_setrow       (blkalign_t, count_t);

#endif

