/*
 * This file is part of zAlign.
 *
 * zAlign is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zAlign is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zAlign.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Header file inclusions */

#ifndef TYPES_H
#define TYPES_H

#define NEGATIVE_INFINITY 0x80000000  /* negative infinity, used with simentry_t variables */
#define ROOT_RANK         0           /* MPI_COMM_WORLD root node rank */


/* Type definitions */

typedef char*           sequence_t;   /* biological sequence */
typedef signed int      simentry_t;   /* cell value in similarity matrix */
typedef signed long int position_t;   /* position inside a biological sequence */
typedef unsigned int    count_t;      /* general length and position counter */

#endif

